<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;

class SupportController extends Controller
{
    public function index()
    {
    	$supports = DB::table('supports')->get();
    	return view('frontend.support.index',compact('supports'));
    }

    public function search($search)
    {
    	if($search == 'all_blank')
    	{
    		$supports = DB::table('supports')->get();
    	}
    	else
    	{
			$supports = DB::table('supports')->where('name','LIKE','%'.$search.'%')->get();
		}
		return view('frontend.support.search',compact('supports'));
    }
}
