<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Auth;
use File;
use Session;
Use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profile()
    {
        $id = Auth::user('web')->id;
        $user = User::find($id);

        return view('frontend.user.profile',compact('user'));
    }

    public function profile_edit()
    {
        $id = Auth::user('web')->id;
        $user = User::find($id);

        return view('frontend.user.profile_edit',compact('user'));
    }

    public function profile_update(Request $request)
    {
        $id = Auth::user('web')->id;

        $this->Validate($request, [
            'username'  =>  'required|min:3',
            'fullname'  =>  'required|min:3',
            'email'     =>  'required|email',
            'gender'    =>  'required',
            'mobile_no' =>  'required|digits:10',
        ]);

        $user = User::find($id);
        $user->username = $request->username;
        $user->fullname = $request->fullname;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->mobile_no = $request->mobile_no;

        if($request->profile != '')
        {
            File::delete(public_path('profile/users/'.$user->profile));

            $imageName = time().'.'.$request->profile->extension();
            $request->profile->move(public_path('profile/users/'), $imageName);

            $user->profile = $imageName;
        }

        $user->save();

        return redirect()->route('user.profile');
    }

    public function change_password()
    {
        return view('frontend.user.change_password');
    }

    public function change_password_update(Request $request)
    {
        $this->Validate($request, [
            'old_password'  =>  'required',
            'new_password'  =>  'required|min:8',
            'confirm_new_password'  =>  'required|same:new_password'
        ]);

        $id = Auth::user()->id;
        $user = User::find($id);

        if(Hash::check($request->old_password, $user->password))
        {
            $user = User::find($user->id)->update(['password' => Hash::make($request->new_password)]);

            session::flash('success','Your password is successfully updated !');
            return redirect('user/change_password');
        }
        else
        {
            session::flash('error','Your corrent password is wrong !');
            return redirect('user/change_password');
        }
    }
}
