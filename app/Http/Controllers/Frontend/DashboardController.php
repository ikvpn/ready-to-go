<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;

class DashboardController extends Controller
{
    public function index()
    {
    	return view('frontend.index');
    }

    public function language($locale)
    {
        Session::put('locale', $locale);
        return redirect()->back();
    }
}
