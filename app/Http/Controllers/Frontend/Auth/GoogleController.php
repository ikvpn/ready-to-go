<?php
namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use Socialite;
use Auth;
use Exception;
use App\User;

class GoogleController extends Controller
{

    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {

        try {

            $user = Socialite::driver('google')->user();

            $finduser = User::where('google_id', $user->id)->first();

            if($finduser){

                Auth::login($finduser);
                return redirect('/');

            }else{

                $newUser = User::create([

                    'username' => $user->user['given_name'],
                    'fullname' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'profile' => $user->avatar,    
                    'password' => encrypt('123456dummy'),
                    'provider' => 'google'

                ]);

                Auth::login($newUser);
                return redirect('/');
            }

        } catch (Exception $e) {

            dd($e->getMessage());

        }
    }
}