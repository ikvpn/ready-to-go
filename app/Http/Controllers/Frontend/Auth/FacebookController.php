<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Socialite;
use Exception;
use Auth;


class FacebookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();

            $finduser = User::where('facebook_id', $user->id)->first();

            if($finduser){

                Auth::login($finduser);
                return redirect('/');

            }else{
                
                $newUser = User::create([

                    'username' => $user->getName(),
                    'fullname' => $user->getName(),
                    'email' => $user->getEmail(),
                    'facebook_id'=> $user->getId(),
                    'profile' => $user->avatar,
                    'password' => encrypt('123456dummy'),
                    'provider' => 'facebook'

                ]);

                Auth::login($newUser);
                return redirect('/');
            }

        } catch (Exception $e) {

            return redirect('/facebook');

        }
    }
}