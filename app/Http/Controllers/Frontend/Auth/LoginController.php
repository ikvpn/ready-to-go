<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Session;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('userlogout');
    }

    public function showLoginForm()
    {
        return view('frontend.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request,[
            'email'     =>  'required|email',
            'password'  =>  'required|min:6'
        ]);

        $user = User::where('email',$request->email)->first();
        if(isset($user) != '')
        {
            if(Auth::guard('web')->attempt(['email'=>$request->email, 'password'=>$request->password],$request->remember))
            {
                return redirect('/');
            }
            else
            {
                Session::flash('login_error','Your email and password is invalid.');
                return redirect('/');
            }
        }
        else
        {
            Session::flash('login_error','Your email is not exits.');
            return redirect('/');
        }

        // return redirect()->back()->withInput($request->only('email','remember'));
    }

    public function userlogout()
    {
        Auth::guard('web')->logout();

        return redirect('/');
    }
}
