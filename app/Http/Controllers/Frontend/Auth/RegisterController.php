<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // return Validator::make($data, [
        //     'username' => ['required', 'string', 'max:191'],
        //     'fullname' => ['required', 'string', 'max:191'],
        //     'email' => ['required', 'string', 'email', 'max:191', 'unique:users'],
        //     'password' => ['required', 'string', 'min:8', 'confirmed'],
        //     'gender' => ['required', 'string', 'max:191'],
        //     'mobile_no' => ['required', 'max:191'],
        //     // 'profile' => ['required', 'string', 'max:191'],
        // ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $imageName = time().'.'.$data['profile']->extension();
        $data['profile']->move(public_path('profile/users/'), $imageName);

        return User::create([
            'username' => $data['username'],
            'fullname' => $data['fullname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'gender' => $data['gender'],
            'mobile_no' => $data['mobile_no'],
            'profile' => $imageName
        ]);
    }

    public function showRegistrationForm()
    {
        return view('frontend.auth.register');
    }

    public function register(Request $request)
    {

        // $this->validator($request->all())->validate();

        $user = User::where('email',$request->email)->first();
        if($user)
        {
            Session::flash('register_error','This email is already exits.');
            return redirect('/');
        }
        else
        {
            event(new Registered($user = $this->create($request->all())));

            $this->guard()->login($user);

            if ($response = $this->registered($request, $user)) {
                return $response;
            }

            return $request->wantsJson()
                        ? new Response('', 201)
                        : redirect($this->redirectPath());
        }
    }
}
