<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;

class BlogController extends Controller
{
    public function index()
    {
    	$blogs = DB::table('blogs')->where('status','active')->orderBy('id','desc')->paginate(6);
    	$countries = DB::table('blogs')->select('country','country_zh')->groupBY('country','country_zh')->get();
    	return view('frontend.blog.index',compact('blogs','countries'));
    }

    public function county($country)
    {
    	$blogs = Db::table('blogs')->where('country',$country)->orderBy('id','desc')->paginate(6);
    	$countries = DB::table('blogs')->select('country')->groupBY('country')->get();
    	return view('frontend.blog.index',compact('blogs','countries'));
    }
}
