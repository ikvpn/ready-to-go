<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;

class CmsController extends Controller
{
    public function why_RTG()
    {
    	$communities = DB::table('cms_why_rtg')->get();
    	return view('frontend.cms.why_RTG', compact('communities'));
    }

    public function start_RTG_point()
    {
    	$points = DB::table('cms_start_RTG_point')->get();
    	$faqs = DB::table('cms_start_rtg_point_faqs')->get();
    	return view('frontend.cms.start_RTG_point', compact('points','faqs'));
    }

    public function travel_country($country)
    {
    	$slider = DB::table('cms_travel_country_slider')->where('title',$country)->first();
    	$navbar = DB::table('travel_posts')->select('country','country_zh')->groupBY('country','country_zh')->get();
    	$recent_posts = DB::table('travel_posts')->orderBy('id','desc')->paginate(5);

    	$posts = DB::table('travel_posts')->where('country',$country)->paginate(9);
    	foreach ($posts as $post) {
    		$post_ids = explode(',',$post->category_id);
    		$all_categories = DB::table('travel_category')->whereIn('id',$post_ids)->get();
    		$post->all_categories = array('all_categories' => $all_categories);
	    }
    	return view('frontend.cms.travel_country', compact('navbar','slider','recent_posts','posts'));
    }

    public function travel_category($category)
    {

    }

    public function promotions()
    {
        $points = DB::table('cms_start_RTG_point')->get();
        $welcome_offers = DB::table('promotion_welcome_offers')->where('status','active')->get();
        $special_offers = DB::table('promotion_special_offers')->where('status','active')->get();
        return view('frontend.cms.promotion',compact('points','welcome_offers','special_offers'));
    }

    public function aboutus()
    {
        return view('frontend.cms.aboutus');
    }

    public function terms_condition($slug)
    {   
        $terms = DB::table('terms_condition')->where('slug',$slug)->first();
        return view('frontend.cms.terms_condition',compact('terms'));
    }

    public function policy($slug)
    {
        $policy = DB::table('terms_condition')->where('slug',$slug)->first();
        return view('frontend.cms.policy',compact('policy'));
    }

    public function press()
    {
        $businesses = DB::table('cms_press')->where('type','business')->where('status','active')->get();
        $events = DB::table('cms_press')->where('type','event')->where('status','active')->get();
        $fundings = DB::table('cms_press')->where('type','fundings')->where('status','active')->get();
        return view('frontend.cms.press',compact('businesses','events','fundings'));
    }
}
