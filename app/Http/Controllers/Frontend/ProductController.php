<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;

class ProductController extends Controller
{
    public function destination($country)
    {
    	$products = DB::table('products')->where('country',$country)->get();
    	$products_count = DB::table('products')->where('country',$country)->count();
    	foreach ($products as $key => $product) {
    		$product->booked_number = $this->number_format_short($product->booked);
    	}
    	
    	$languages = DB::table('languages')->where('status','active')->get();
    	$search = $country;

    	return view('frontend.product.destination',compact('products','products_count','languages','search'));
    }

    public function number_format_short( $n, $precision = 1 ) {
		if ($n < 900) {
			// 0 - 900
			$n_format = number_format($n, $precision);
			$suffix = '';
		} else if ($n < 900000) {
			// 0.9k-850k
			$n_format = number_format($n / 1000, $precision);
			$suffix = 'K';
		} else if ($n < 900000000) {
			// 0.9m-850m
			$n_format = number_format($n / 1000000, $precision);
			$suffix = 'M';
		} else if ($n < 900000000000) {
			// 0.9b-850b
			$n_format = number_format($n / 1000000000, $precision);
			$suffix = 'B';
		} else {
			// 0.9t+
			$n_format = number_format($n / 1000000000000, $precision);
			$suffix = 'T';
		}

	  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
	  // Intentionally does not affect partials, eg "1.50" -> "1.50"
		if ( $precision > 0 ) {
			$dotzero = '.' . str_repeat( '0', $precision );
			$n_format = str_replace( $dotzero, '', $n_format );
		}

		return $n_format . $suffix;
	}

	public function filter_destination(Request $request)
	{
		dd($requst->lang);
	}
}
