<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
p|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.index');
// });
Route::get('/','Frontend\DashboardController@index');

Auth::routes();

// manage lnaguage 
Route::get('locale/{locale}','Frontend\DashboardController@language')->name('language');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/register','Frontend\Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register','Frontend\Auth\RegisterController@register')->name('register.submit');
Route::get('/login','Frontend\Auth\LoginController@showLoginForm')->name('login');
Route::post('/login','Frontend\Auth\LoginController@login')->name('login.submit');
Route::get('/user/userlogout', 'Frontend\Auth\LoginController@userlogout')->name('user.logout');

Route::get('password/reset', 'Frontend\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Frontend\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Frontend\Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Frontend\Auth\ResetPasswordController@reset')->name('password.update');


Route::get('user/profile','Frontend\UserController@profile')->name('user.profile');
Route::get('user/profile/edit','Frontend\UserController@profile_edit')->name('user.profile.edit');
Route::post('user/profile/update','Frontend\UserController@profile_update')->name('user.profile.update');

Route::get('user/change_password','Frontend\UserController@change_password')->name('user.change_password');
Route::post('user/change_password','Frontend\UserController@change_password_update')->name('user.change_password.submit');

Route::get('/google', 'Frontend\Auth\GoogleController@redirectToGoogle');
Route::get('/google/callback', 'Frontend\Auth\GoogleController@handleGoogleCallback');

Route::get('/facebook', 'Frontend\Auth\FacebookController@redirectToFacebook');
Route::get('/facebook/callback', 'Frontend\Auth\FacebookController@handleFacebookCallback');


Route::get('/admin', 'AdminController@index')->name('admin.dashboard');

Route::get('/admin/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login','Auth\AdminLoginController@login')->name('admin.login.submit');

Route::get('/admin/logout','Auth\AdminLoginController@logout')->name('admin.logout');



Route::get('support', 'Frontend\SupportController@index')->name('frontend.support');
Route::get('support/search/{search}', 'Frontend\SupportController@search')->name('frontend.support.search');

Route::get('why_RTG','Frontend\CmsController@why_RTG')->name('why_RTG');
Route::get('start_RTG_point','Frontend\CmsController@start_RTG_point')->name('start_RTG_point');

Route::get('blog','Frontend\BlogController@index')->name('blog');
Route::get('blog/{county}','Frontend\BlogController@county')->name('blog.country');

Route::get('travel_country/{country}','Frontend\CmsController@travel_country')->name('travel_country');
Route::get('travel_category/{category}','Frontend\CmsController@travel_category')->name('travel_category');

Route::get('promotions','Frontend\CmsController@promotions')->name('promotions');
Route::get('aboutus','Frontend\CmsController@aboutus')->name('aboutus');
Route::get('terms_condition/{slug}','Frontend\CmsController@terms_condition')->name('terms_condition');
Route::get('policy/{slug}','Frontend\CmsController@policy')->name('policy');
Route::get('press','Frontend\CmsController@press')->name('press');

Route::get('destination/{country}','Frontend\ProductController@destination')->name('destination');
Route::post('filter_destination','Frontend\ProductController@filter_destination')->name('filter_destination');
