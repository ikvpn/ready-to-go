<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id' => '466549748083-2f2k2hav2uck3u5ficapiivc9es5fd99.apps.googleusercontent.com',
        'client_secret' => 'Ue38g5en-sRfNuVj2wePN3_m',
        'redirect' => 'http://localhost/readyToGo/public/google/callback',
    ],

    'facebook' => [
        'client_id' => '207648133896773',
        'client_secret' => '2013ca503701c364da58064ad0451186',
        'redirect' => 'http://localhost/readyToGo/public/facebook/callback',
    ],

];
