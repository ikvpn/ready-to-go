<!DOCTYPE html>
<html class="no-js" lang="zxx">

<!-- Mirrored from ecologytheme.com/theme/travelstar/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 03 Apr 2020 05:26:10 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="TravelStar - Tour, Travel, Travel Agency Template">
    <meta name="keywords" content="Tour, Travel, Travel Agency Template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TravelStar - Tour, Travel & Travel Agency Template</title>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('theme/images/favicon.ico') }}">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/bootstrap.min.css') }}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/animate.css') }}">
    <!-- Button Hover animate css -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/hover-min.css') }}">
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/jquery-ui.min.css') }}">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/meanmenu.min.css') }}">
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/owl.carousel.min.css') }}">
    <!-- slick css -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/slick.css') }}">
    <!-- chosen.min-->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/jquery-customselect.css') }}">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/font-awesome.min.css') }}">
    <!-- magnific Css -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/magnific-popup.css') }}">
    <!-- Revolution Slider -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/revolution/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/assets/revolution/navigation.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/assets/revolution/settings.css') }}">
    <!-- Preloader css -->
    <link rel="stylesheet" href="{{ asset('theme/css/assets/preloader.css') }}"> 
    <!-- custome css -->
    <link rel="stylesheet" href="{{ asset('theme/css/style.css') }}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('theme/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/master.css') }}">
    <!-- modernizr css -->

    @stack('header-script')

    <script src="{{ asset('theme/js/vendor/modernizr-2.8.3.min.js') }}"></script>

    <style type="text/css">
    	.error {
    		color: #ff0000;
    	}
    </style>
</head>
<body> 
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- header area start here -->
<header>
	<div class="header_top_area">
		<div class="container">
			<div class="row">
				<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="contact_wrapper_top">
						<ul class="header_top_contact">
							<li><i class="fa fa-phone" aria-hidden="true"></i>+123-456-7890</li>
							<li><i class="fa fa-envelope-o" aria-hidden="true"></i>info@yourcompany.com</li>
						</ul>
						<div class="book-btn">
							<a href="#">Book Now</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- header top end -->

	<div class="main_nav">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-sm-2 col-xs-12 tap-v-responsive">
					<div class="logo-area">
						<a href="index-2.html"><img src="{{ asset('theme/images/logo.png') }}" alt="">
						</a>
					</div>
				</div>
				<div class="col-md-10">
					<nav>
						<ul class="main-menu text-right">
							<li class="active"><a href="index-2.html">Home</a>
								<ul class="dropdown">
									<li><a href="index-2.html">Home V1</a></li>
									<li><a href="index-3.html">Home V2</a></li>
									<li><a href="index-4.html">Home V3</a></li>
									<li><a href="index-5.html">Home V4</a></li>
								</ul>
							</li>
							<li><a href="package-version-one.html">Package List</a>
								<ul class="dropdown">
									<li><a href="package-version-one.html">Package One</a></li>
									<li><a href="package-version-two.html">Package Two</a></li>
									<li><a href="single-package.html">Package Details</a></li>
								</ul>
							</li>
							<li><a href="hotel-version-one.html">Hotels</a>
								<ul class="dropdown">
									<li><a href="hotel-version-one.html">Hotel One</a></li>
									<li><a href="hotel-version-two.html">Hotel Two</a></li>
									<li><a href="hotel-details.html">Hotel Details</a></li>
								</ul>
							</li>
							<li><a href="#">Flights</a></li>
							<li><a href="blog-version-one.html">Blog</a>
								<ul class="dropdown">
									<li><a href="blog-version-one.html">Blog One</a></li>
									<li><a href="blog-version-two.html">Blog Two</a></li>
									<li><a href="blog-single.html">Blog Post</a></li>
								</ul>
							</li>
							<li><a href="#">Pages</a>
								<ul class="dropdown">
									<li><a href="package-version-one.html">Package One</a></li>
									<li><a href="package-version-two.html">Package Two</a></li>
									<li><a href="single-package.html">single package</a></li>
									<li><a href="hotel-version-one.html">Hotel One</a></li>
									<li><a href="hotel-version-two.html">Hotel Two</a></li>
									<li><a href="blog-version-one.html">Blog One</a></li>
									<li><a href="hotel-version-two.html">Blog Two </a></li>
									<li><a href="blog-single.html">Single Blog</a></li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</li>
							<li><a href="contact.html">Contact</a></li>

							@if(isset(Auth::user()->id) != '')
							<li><a href="javascript:void(0);">{{ ucwords(Auth::user()->username) }}</a>
								<ul class="dropdown">
									<li><a href="{{ route('user.profile') }}">Profile</a></li>
									@if(Auth::user('web')->provider == '')
										<li><a href="{{ route('user.change_password') }}">Change Password</a></li>
									@endif
									<li><a href="{{ route('user.logout') }}">Logout</a></li>
								</ul>
							</li>
							@else
							<li><a href="javascript:void(0);" id="sign_in">Sign In/Sign up</a></li>
							@endif
						</ul>
					</nav>
				</div> <!-- main menu end here -->
			</div>
		</div>
	</div> <!-- header-bottom area end here -->
</header> <!-- header area end here -->

<div class="modal" id="sign_in_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	@if(Session::get('login_error'))
      	<input type="hidden" name="login_error" id="login_error" @if(Session::get('login_error')) value="1" @endif>
      	<div class="row">
      		<div class="col-md-12">
      			<div class="alert alert-danger">{{Session::get('login_error')}}</div>
      		</div>
      	</div>
      	@endif
      	<form method="post" action="{{ route('login') }}" id="sign_in_form">
      		@csrf
	      	<div class="form-group">
		        <label>Email:</label>
		        <input type="email" name="email" id="email" class="form-control">
	      	</div>
	      	<div class="form-group">
		        <label>Password:</label>
		        <input type="password" name="password" id="password" class="form-control">
		    </div>
        <a href="javascript:void(0);" id="sign_up">Create new account ?</a><br>
        <a href="javascript:void(0);" id="forgot_pass">Forgot your password ?</a>
      </div>
      <div class="modal-footer">
  		<div class="row">
  			<div class="col-md-12">
			      	<a href="{{ url('/google') }}" class="btn btn-success btn-block">
				      Login With Google
				    </a>
  			</div>
      		<dir class="col-md-12">
		        <a href="{{ url('/facebook') }}" class="btn btn-success btn-block">
			      Login With Facebook
			    </a>
      		</dir>
      		<div class="col-md-12 text-right">
				<button type="submit" class="btn btn-primary">Submit</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><br>
      		</div>
  		</div>
      	</div>
      </div>
      	</form>
    </div>
  </div>
</div>

<div class="modal" id="sign_up_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Register</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	@if(Session::get('register_error'))
      	<input type="hidden" name="register_error" id="register_error" @if(Session::get('register_error')) value="1" @endif>
      	<div class="row">
      		<div class="col-md-12">
      			<div class="alert alert-danger">{{Session::get('register_error')}}</div>
      		</div>
      	</div>
      	@endif
      	<form method="post" enctype="multipart/form-data" action="{{ route('register.submit') }}" id="sign_up_form">
      		@csrf
      		<div class="form-group">
      			<div class="row">
      				<div class="col-md-6">
				        <label>User Name:</label>
				        <input type="text" name="username" id="username" class="form-control">
      				</div>
      				<div class="col-md-6">
				        <label>Full Name:</label>
				        <input type="text" name="fullname" id="fullname" class="form-control">
      				</div>
      			</div>
	      	</div>
	      	<div class="form-group">
      			<div class="row">
      				<div class="col-md-6">
				        <label>Email:</label>
				        <input type="email" name="email" id="email" class="form-control">
				    </div>
      				<div class="col-md-6">
				        <label>Password:</label>
				        <input type="password" name="password" id="register_password" class="form-control">
      				</div>
				</div>
	      	</div>
	      	<div class="form-group">
      			<div class="row">
      				<div class="col-md-6">
				        <label>Confirm Password:</label>
		        		<input id="password-confirm" type="password" class="form-control" name="password_confirmation">
		        	</div>
      				<div class="col-md-6">
				    	<label>Gender:</label><br>
				    	<input type="radio" name="gender" value="male"> Male
				    	<input type="radio" name="gender" value="female"> Female<br>
      				</div>
		        </div>
		    </div>
	      	<div class="form-group">
      			<div class="row">
      				<div class="col-md-6">
				        <label>Mobile No:</label><br>
				        <input type="text" name="mobile_no" id="mobile_no" class="form-control">
				    </div>
      				<div class="col-md-6">
				        <label>Profile:</label>
				        <input type="file" name="profile" id="profile">
      				</div>
				</div>
	      	</div>
        <a href="javascript:void(0);" id="sing_up_to_sign_in">I have already account.</a>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
  		</form>
    </div>
  </div>
</div>

<div class="modal" id="forgot_pass_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Forgot password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	@if(Session::get('forgot_pass_error'))
      	<input type="hidden" name="forgot_pass_error" id="forgot_pass_error" @if(Session::get('forgot_pass_error')) value="1" @endif>
      	<div class="row">
      		<div class="col-md-12">
      			<div class="alert alert-danger">{{Session::get('forgot_pass_error')}}</div>
      		</div>
      	</div>
      	@endif
      	<form method="post" action="{{ route('password.email') }}" id="forgot_pass_form">
      		@csrf
	      	<div class="form-group">
		        <label>Email:</label>
		        <input type="email" name="email" id="email" class="form-control">
	      	</div>
        <a href="javascript:void(0);" id="forgot_pass_to_sign_in">I have already account.</a>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
  		</form>
    </div>
  </div>
</div>


	@yield('content')


<footer class="footer-area">
	<div class="container">
		<div class="row">
			<!-- footer left -->
			<div class="col-12 col-sm-6 col-md-6 col-lg-3">
				<div class="single-footer">
					<div class="footer-title">
						<h3><a href="#"><img src="{{ asset('theme/images/logo.png') }}" alt=""></a></h3>
					</div>
					<div class="footer-left">
						<div class="footer-logo">
							<p>Lorem ipsum dolor sit amet conset ctetur adipiscin elit Etiam at ipsum at ligula vestibulum sodales.</p>
						</div>
						<ul class="footer-contact">
							<li><img class="map" src="{{ asset('theme/images/icon/map.png') }}" alt="">Seventh Avenue New York</li>
							<li><img class="map" src="{{ asset('theme/images/icon/phone.png') }}" alt="">+123-456-7890</li>
							<li><img class="map" src="{{ asset('theme/images/icon/gmail.png') }}" alt="">info@yourcompany.com</li>
						</ul>
					</div>
				</div>
			</div> <!-- footer left -->

			<!-- footer destination -->
			<div class="col-12 col-sm-6 col-md-6 col-lg-2">
				<div class="single-footer">
					<div class="footer-title">
						<h3>Quick Link</h3>
					</div>
					<ul class="list-unstyled">
						<li><a href="#" title="">Home</a></li>
						<li><a href="#" title="">Package List</a></li>
						<li><a href="#" title="">Hotels</a></li>
						<li><a href="#" title="">Flights</a></li>
						<li><a href="#" title="">Blog</a></li>
						<li><a href="#" title="">Pages</a></li>
						<li><a href="#" title="">Contact</a></li>
					</ul>
				</div>
			</div>	<!-- footer destination -->

			<div class="col-12 col-sm-6 col-md-6 col-lg-4">
				<div class="single-footer">
					<div class="single-recent-post">
						<div class="footer-title">
							<h3>Recent News</h3>
						</div>
						<ul class="recent-post">
							<li>
								<a href="#">
									<div class="post-thum">
										<img src="{{ asset('theme/images/blog/f4.jpg') }}" alt="" class="img-rounded">
									</div>
									<div class="post-content">
										<p>A Clean Website Gives More Experience To The Visitors.
										</p>
										<span>12 July, 2019</span>
									</div>
								</a>
							</li>
							<li>
								<a href="#">
									<div class="post-thum">
										<img src="{{ asset('theme/images/blog/f5.jpg') }}" alt="" class="img-rounded">
									</div>
									<div class="post-content">
										<p>A Clean Website Gives More Experience To The Visitors.
										</p>
										<span>12 July, 2019</span>
									</div>
								</a>
							</li>
							<li>
								<a href="#">
									<div class="post-thum">
										<img src="{{ asset('theme/images/blog/f6.jpg') }}" alt="" class="img-rounded">
									</div>
									<div class="post-content">
										<p>A Clean Website Gives More Experience To The Visitors.
										</p>
										<span>12 July, 2019</span>
									</div>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>	<!-- footer latest news -->

			<!-- footer contact -->
			<div class="col-12 col-sm-6 col-md-6 col-lg-3 f-phone-responsive">
				<div class="single-footer">
					<div class="footer-title">
						<h3>Quick Contact</h3>
					</div>
					<div class="footer-contact-form">
						<form action="#">
							<ul class="footer-form-element">
								<li>
									<input type="text" name="email" id="email" placeholder="" value="Email Address" onblur="if(this.value==''){this.value='Email Address'}" onfocus="if(this.value=='Email Address'){this.value=''}">
								</li>
								<li class="text_area">
									<textarea name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
									<button type="submit">Send</button>
								</li>
								<li>
									
								</li>
							</ul>
						</form>
					</div>
					<div class="footer-social-media">
						<div class="social-footer-title">
							<h3>Follow Us</h3>
						</div>
						<ul class="footer-social-link">
							<li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li class="gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
						</ul>
					</div>
				</div>
			</div>	<!-- footer contact -->
		</div>
	</div>
	<div class="footer_bottom_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12">
					<div class="copy_right_wrapper">
						<div class="copyright">
							<p>Copyright &copy; 2019 Travelstar By Ecology Theme</p>
						</div>
						<ul class="payicon pull-right">
							<li>We Accept : </li>
							<li><a href="#" title=""><img src="{{ asset('theme/images/payicon01.png') }}" alt=""></a></li>
							<li><a href="#" title=""><img src="{{ asset('theme/images/payicon02.png') }}" alt=""></a></li>
							<li><a href="#" title=""><img src="{{ asset('theme/images/payicon03.png') }}" alt=""></a></li>
							<li><a href="#" title=""><img src="{{ asset('theme/images/payicon04.png') }}" alt=""></a></li>
							<li><a href="#" title=""><img src="{{ asset('theme/images/payicon05.png') }}" alt=""></a></li>
							<li><a href="#" title=""><img src="{{ asset('theme/images/payicon06.png') }}" alt=""></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer> <!-- end footer -->

<div class="to-top pos-rtive">
	<a href="#"><i class = "fa fa-angle-up"></i></a>
</div><!-- Scroll to top-->
    
    <!-- ============================
    		JavaScript Files
    ============================= -->
    <!-- jquery -->
    <script src="{{ asset('theme/js/vendor/jquery-3.2.0.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
    <!-- bootstrap js -->
    <script src="{{ asset('theme/js/bootstrap.min.js') }}"></script>    
    <script src="{{ asset('theme/js/popper.min.js') }}"></script>
    <!-- owl.carousel js -->
    <script src="{{ asset('theme/js/owl.carousel.min.js') }}"></script>
    <!-- slick js -->
    <script src="{{ asset('theme/js/slick.min.js') }}"></script>
    <!-- meanmenu js -->
    <script src="{{ asset('theme/js/jquery.meanmenu.min.js') }}"></script>
    <!-- jquery-ui js -->
    <script src="{{ asset('theme/js/jquery-ui.min.js') }}"></script>
    <!-- wow js -->
    <script src="{{ asset('theme/js/wow.min.js') }}"></script>
    <!-- counter js -->
    <script src="{{ asset('theme/js/jquery.counterup.min.js') }}"></script>
    <!-- Countdown js -->
    <script src="{{ asset('theme/js/jquery.countdown.min.js') }}"></script>
    <!-- waypoints js -->
    <script src="{{ asset('theme/js/jquery.waypoints.min.js') }}"></script>
    <!-- Isotope js -->
    <script src="{{ asset('theme/js/isotope.pkgd.min.js') }}"></script>
    <!-- magnific js -->
    <script src="{{ asset('theme/js/jquery.magnific-popup.min.js') }}"></script>
    <!-- Image loaded js -->
    <script src="{{ asset('theme/js/imagesloaded.pkgd.min.js') }}"></script>
    <!-- chossen js -->
    <script src="{{ asset('theme/js/chosen.jquery.min.js') }}"></script>
    <!-- Revolution JS -->
    <script src="{{ asset('theme/js/assets/revolution/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/extensions/revolution.extension.video.min.js') }}"></script>
    <script src="{{ asset('theme/js/assets/revolution/revolution.js') }}"></script>
    <!-- plugin js -->
    <script src="{{ asset('theme/js/plugins.js') }}"></script>
    <!-- select2 js -->
    <script src="{{ asset('theme/js/select2.min.js') }}"></script>    
    <script src="{{ asset('theme/js/colors.js') }}"></script>
    <!-- customSelect Js -->
    <script src="{{ asset('theme/js/jquery-customselect.js') }}"></script>
    <!-- custom js -->
    <script src="{{ asset('theme/js/custom.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

    <script type="text/javascript">

    	$("#sign_in_form").validate({
		  rules: {
		    email: {
		      required: true,
		      email: true
		    },
		    password: {
		      required: true,
		      minlength: 8
		    }
		  },
		  messages: {
		  	email: {
		  		required: "Email field is required",
		  	},
		    password: {
		      	required: "Password field is required",
		    }
		  }
		});

		$("#sign_up_form").validate({
			errorPlacement: function(error, element) {
		        error.appendTo( element.parent("div") );
		        // error.appendTo( element.parent("div").next("div") );
		    },
		  rules: {
		  	username: {
		      required: true,
		      minlength: 3
		    },
		    fullname: {
		      required: true,
		      minlength: 3
		    },
		    email: {
		      required: true,
		      email: true
		    },
		    password: {
		      required: true,
		      minlength: 8
		    },
		    password_confirmation: {
              equalTo: "#register_password"
		    },
		    gender: {
		      required: true,
		    },
		    mobile_no: {
		      required: true,
		      digits: true,
		      minlength: 10,
		      maxlength: 10
		    },
		    profile: {
		    	required: true,
		    	extension: "png|jpg|jpeg",
		    	filesize: 1048576
		    }
		  },
		  messages: {
		  	username: {
		  		required: "User Name field is required.",
		  	},
		  	fullname: {
		  		required: "Full Name field is required.",
		  	},
		  	email: {
		  		required: "Email field is required.",
		  	},
		    password: {
		      	required: "Password field is required.",
		    },
		    password_confirmation: {
		  		required: "Confirm password field is required.",
		  		equalTo: "Your password and confirm password is not match."
		  	},
		  	gender: {
		      required: "Gender field is required.",
		    },
		    mobile_no: {
		      required: "Mobile No field is required.",
		    },
		    profile: {
		    	required: "Profile field is required.",
		    }
		  }
		});

		$("#forgot_pass_form").validate({
		  rules: {
		    email: {
		      required: true,
		      email: true
		    }
		  },
		  messages: {
		  	email: {
		  		required: "Email field is required",
		  	}
		  }
		});
    	
    	$('#sign_in').on('click', function(){
    		$('#sign_in_modal').modal('show');
    	});

    	$('#sign_up').on('click', function(){
    		$('#sign_in_modal').modal('hide');
    		$('#sign_up_modal').modal('show');
    	});

    	$('#forgot_pass').on('click', function(){
    		$('#sign_in_modal').modal('hide');
    		$('#forgot_pass_modal').modal('show');
    	});

    	$('#sing_up_to_sign_in').on('click', function(){
    		$('#sign_up_modal').modal('hide');
    		$('#sign_in_modal').modal('show');
    	});

    	$('#forgot_pass_to_sign_in').on('click', function(){
    		$('#forgot_pass_modal').modal('hide');
    		$('#sign_in_modal').modal('show');
    	});

    	setTimeout(function(){
    		if ($('#login_error').val() == 1) {
	    		$('#sign_in_modal').modal('show');
	    	}
	    	if ($('#register_error').val() == 1) {
	    		$('#sign_up_modal').modal('show');
	    	}
	    	if($('#forgot_pass_error').val() == 1) {
	    		$('#forgot_pass_modal').modal('show');
	    	}
    	}, 5000);

    </script>

    @stack('footer-script')
</body>

<!-- Mirrored from ecologytheme.com/theme/travelstar/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 03 Apr 2020 05:29:23 GMT -->
</html>
