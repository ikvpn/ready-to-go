<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ready to go</title>
	<link href="{{ asset('theme1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('theme1/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/responsive.css') }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
</head>
<body>


<!-- header start -->    

<header class="sticky-top search-part">
<nav class="navbar navbar-expand-lg navbar-light">
     <div class="container">
  <a class="navbar-brand" href="#"><img src="{{ asset('theme1/images/logo.png') }}" alt=""></a></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> 
   <div class="search-header">                     
                     <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search by destination, activity, or interest">
                        <div class="input-group-append">
                          <button class="btn btn-default pl-3 pr-3" type="button">
                            <i class="fa fa-search" aria-hidden="true"></i>
                          </button>
                        </div>
                      </div>                      
                  </div>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#"><i class="fa fa-mobile" aria-hidden="true"></i> Get Exclusive App Deals</a>
      </li>
         <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        English
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Chines</a>
          <a class="dropdown-item" href="#"></a>
        </div>
      </li>
         <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        HKD
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">HKD-1</a>
          <a class="dropdown-item" href="#">HKD-2</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
      </li>
       <li class="nav-item pt-2">
        <a class="d-inline nav-link" href="#">Login</a>/<a class="d-inline nav-link" href="#">Sign Up</a>
      </li>
        
    </ul>    
  </div>
</div>
</nav>
</header>

<!-- header end -->

<!-- why RTG start -->

<section class="destination-part ptg-50">
<div class="container">       
            <div class="row">                
                <div class="col-lg-6 col-md-12 col-sm-12">
                      <div class="rtg-part">
                      <nav aria-label="breadcrumb p-0">
                        <ol class="breadcrumb p-0">
                          <li class="breadcrumb-item"><a href="#">Home</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Taiwan</li>
                        </ol>
                      </nav>
                      <h4 class="f-w-700">Search results "<span class="text-reds">{{ $search }}</span>"</h4>
                  </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                  <div class="">
                   
                  </div>
                </div>               
            </div>
</div>
</section>

<!-- hero-section-1 end -->

<!-- service-section srart -->
<section class="destination-section pbb-50">
    <div class="container"> 
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="side-parts">
                 <h5 class="f-w-700">Destination</h5>
                 <a href="#" class="btn primary-line mr-1">Taipei</a>
                  <a href="#" class="btn primary-line mt-2">New Taipei City</a>
                   <a href="#" class="btn primary-line mt-2">Taichung</a>
                    <a href="#" class="btn primary-line mt-2">Kaohsiung</a>
                     <a href="#" class="btn primary-line mt-2">Hualien</a>
                      <a href="#" class="btn primary-line mt-2">Nantou</a>
                       <a href="#" class="btn primary-line mt-2">Taoyuan</a>
                        <a href="#" class="btn primary-line mt-2">Yilan</a>
                          <a href="#" class="btn btn-success mt-2">More</a>
                </div> 
                 <div class="side-parts mt-4">
                 <h5 class="f-w-700">Categories</h5>
                  <!--Accordion wrapper-->
                    <div class="accordion md-accordion" id="accordionEx1" role="tablist" aria-multiselectable="true">

                      <!-- Accordion card -->
                      <div class="card">

                        <!-- Card header -->
                        <div class="card-header" role="tab" id="headingTwo1">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo1"
                            aria-expanded="false" aria-controls="collapseTwo1">
                            <h5 class="mb-0">Activities        
                              <i class="fa fa-angle-down rotate-ico float-right" aria-hidden="true"></i>
                            </h5>
                          </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseTwo1" class="collapse" role="tabpanel" aria-labelledby="headingTwo1"
                          data-parent="#accordionEx1">
                          <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                            wolf
                            moon       
                          </div>
                        </div>

                      </div>
                      <!-- Accordion card -->

                      <!-- Accordion card -->
                      <div class="card">

                        <!-- Card header -->
                        <div class="card-header" role="tab" id="headingTwo2">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo21"
                            aria-expanded="false" aria-controls="collapseTwo21">
                            <h5 class="mb-0">
                           Attractions & Tickets
                              <i class="fa fa-angle-down rotate-ico float-right" aria-hidden="true"></i>
                            </h5>
                          </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseTwo21" class="collapse" role="tabpanel" aria-labelledby="headingTwo21"
                          data-parent="#accordionEx1">
                          <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                            wolf
                           
                          </div>
                        </div>

                      </div>
                      <!-- Accordion card -->

                      <!-- Accordion card -->
                      <div class="card">

                        <!-- Card header -->
                        <div class="card-header" role="tab" id="headingThree31">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseThree31"
                            aria-expanded="false" aria-controls="collapseThree31">
                            <h5 class="mb-0">
                             Experiences
                              <i class="fa fa-angle-down rotate-ico float-right" aria-hidden="true"></i>
                            </h5>
                          </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseThree31" class="collapse" role="tabpanel" aria-labelledby="headingThree31"
                          data-parent="#accordionEx1">
                          <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                            wolf
                         
                          </div>
                        </div>

                      </div>
                      <!-- Accordion card -->

                        <!-- Accordion card -->
                      <div class="card">

                        <!-- Card header -->
                        <div class="card-header" role="tab" id="headingThree39">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseThree39"
                            aria-expanded="false" aria-controls="collapseThree39">
                            <h5 class="mb-0">
                             Tours
                              <i class="fa fa-angle-down rotate-ico float-right" aria-hidden="true"></i>
                            </h5>
                          </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseThree39" class="collapse" role="tabpanel" aria-labelledby="headingThree39"
                          data-parent="#accordionEx1">
                          <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                            wolf
                            moon
                          
                          </div>
                        </div>

                      </div>
                      <!-- Accordion card -->

                        <!-- Accordion card -->
                      <div class="card">

                        <!-- Card header -->
                        <div class="card-header" role="tab" id="headingThree40">
                          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseThree40"
                            aria-expanded="false" aria-controls="collapseThree40">
                            <h5 class="mb-0">
                           Transport & Essentials
                              <i class="fa fa-angle-down rotate-ico float-right" aria-hidden="true"></i>
                            </h5>
                          </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseThree40" class="collapse" role="tabpanel" aria-labelledby="headingThree39"
                          data-parent="#accordionEx1">
                          <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                            wolf
                            moon
                            
                          </div>
                        </div>

                      </div>
                      <!-- Accordion card -->

                    </div>
<!-- Accordion wrapper -->
                </div> 

            <div class="side-parts mt-4">
                  <h5 class="f-w-700">Available Language(s)</h5>
                  <ul>
                    <li>
                        <label class="check-parts">All
	                      <input type="checkbox" id="language_all">
	                      <span class="checkmark"></span>
	                    </label>
                    </li>
                    @foreach($languages as $language)
                     <li>
                      	<label class="check-parts">{{ $language->language_name }}
		                      <input type="checkbox" class="language" value="{{ $language->id }}">
		                      <span class="checkmark"></span>
	                    </label>
                    </li>
                    @endforeach
                  </ul>
            </div>

               <div class="side-parts mt-4">
                  <h5 class="f-w-700">Budget (HKD)</h5>
                  <div class="price-range-slider">
  
                    <p class="range-value">
                      <input type="text" id="amount" readonly>
                    </p>
                    <div id="slider-range" class="range-bar"></div>
                    
                  </div>
               
            </div>

             <div class="side-parts mt-4">
                  <h5 class="f-w-700">Duration</h5>
                  <ul>
                    <li>
                      <label class="check-parts">0 hours - 4 hours
                      <input type="checkbox">
                      <span class="checkmark"></span>
                    </label>
                    </li>
                       <li>
                      <label class="check-parts">4 hours - 1 Day
                      <input type="checkbox">
                      <span class="checkmark"></span>
                    </label>
                    </li>
                          <li>
                      <label class="check-parts">1 Day - 2 Days
                      <input type="checkbox">
                      <span class="checkmark"></span>
                    </label>
                    </li>
                         <li>
                      <label class="check-parts">2 Days more
                      <input type="checkbox">
                      <span class="checkmark"></span>
                    </label>
                    </li>
                  </ul>
            </div>
          

            </div>
             <div class="col-lg-8 col-md-8 col-sm-12"> 
              <h5 class="f-w-700">Be Inspired in {{ $search }}</h5>              
                 <div class="row">
                   <div class="col-md-4">                     
                      <a href="#" class="destination-part">
                      <img src="{{ asset('theme1/images/taiwan.png') }}" alt="">
                      <h6>Experiences</h6>
                      </a>
                   </div>
                     <div class="col-md-4">                     
                      <a href="#" class="destination-part">
                      <img src="{{ asset('theme1/images/taiwan.png') }}" alt="">
                      <h6>Transport & Essentials</h6>
                      </a>
                   </div>
                     <div class="col-md-4">                     
                      <a href="#" class="destination-part">
                      <img src="{{ asset('theme1/images/taiwan.png') }}" alt="">
                      <h6>Top Sights</h6>
                      </a>
                   </div>

                    <div class="col-lg-12" id="filter_product">
                      <div class="">
                        <div class="taiwan-top mt-4 mb-4">
                       <h5 class="titles pb-3"><span class="text-reds">{{ $products_count }}</span> @lang('product/destination.experiences found')</h5>
                       <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                         <span class="pt-3"> @lang('product/destination.Sort by') :</span> <li class="nav-item f-w-70">
                            <a class="nav-link active f-w-70" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fa fa-flag-o" aria-hidden="true"></i>  @lang('product/destination.RTG suggestions')</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fa fa-flag-o" aria-hidden="true"></i> @lang('product/destination.Popularity')</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">@lang('product/destination.Ratings') <i class="fa fa-star" aria-hidden="true"></i></a>
                          </li>

                           <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">@lang('product/destination.Price: low to high') <i class="fa fa-ticket" aria-hidden="true"></i></a>
                          </li>
                        </ul>
                      </div>
                        <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        	<?php $count = 0 ?>
                        	@forelse($products as $product)
	                        	@if($count == 0)
	                            <div class="experiences-section">
	                              <div class="row">
		                              <div class="col-md-4">
		                                <a href="#" class="img-part">
		                                  <img src="{{ asset('theme1/images/'.$product->image) }}" alt="">
		                                  <span class="span1"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
		                                </a>
		                              </div>
		                              <div class="col-md-8">
		                                <div class="press-text pt-4 pb-4 pr-2">                                  
		                                  <h4>@if(app()->getLocale() == 'en') {{ $product->title }} @else {{ $product->title_zh }} @endif</h4>
		                                  <p class="">@if(app()->getLocale() == 'en') {{ $product->discription }} @else {{ $product->discription_zh }} @endif</p>
		                                  <ul class="mt-2">
		                                      <li class="d-inline pr-4"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> @if(app()->getLocale() == 'en') {{ $product->location }} @else {{ $product->location_zh }} @endif</a></li>
		                                      <li class="d-inline pr-4"><i class="fa fa-bolt" aria-hidden="true"></i> {{$product->booked_number}} @lang('product/destination.booked')</li>
		                                      <li class="d-inline pr-5">                   
		                                          <span class=""><i class="text-warning fa fa-star"></i></span>
		                                            <span class=""><i class="text-warning fa fa-star"></i></span>
		                                          <span class=""><i class="text-warning fa fa-star"></i></span>
		                                          <span class=""><i class="text-warning fa fa-star"></i></span>
		                                           <span class=""><i class="text-warning fa fa-star"></i></span>
		                                            (2510) 
		                                     </li>
		                                      <li class="d-inline"><i class="fa fa-calendar" aria-hidden="true"></i> @lang('product/destination.Earliest Booking Date :') {{ \carbon\carbon::parse($product->date_time)->format('Y-m-d') }}</li>
		                                  </ul>
		                                  <div class="tress-inner mt-3">
		                                    <div class="tress-inner-1">
		                                      <a href="#" class="btn btn-default">@lang('product/destination.Instant Confirmation')</a>
		                                      <a href="#" class="btn btn-default">@lang('product/destination.Free cancellation')</a>
		                                    </div>
		                                    <div class="tress-inner-2">
		                                     <h5 class="text-right">HKD <span class="text-red">{{ $product->price }}</span></h5>
		                                    </div>
		                                  </div>

		                                </div>
	                               </div>
	                              </div>
	                            </div>
	                            @else
	                            <div class="experiences-section mt-4">
	                              <div class="row">
	                              <div class="col-md-4">
	                                <a href="#" class="img-part">
	                                  <img src="{{ asset('theme1/images/'.$product->image) }}" alt="">
	                                  <span class="span1"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
	                                </a>
	                              </div>
	                              <div class="col-md-8">
	                                <div class="press-text pt-4 pb-4 pr-2">                                  
	                                  <h4>@if(app()->getLocale() == 'en') {{ $product->title }} @else {{ $product->title_zh }} @endif</h4>
		                              <p class="">@if(app()->getLocale() == 'en') {{ $product->discription }} @else {{ $product->discription_zh }} @endif</p>
	                                  <ul class="mt-2">
	                                      <li class="d-inline pr-4"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> @if(app()->getLocale() == 'en') {{ $product->location }} @else {{ $product->location_zh }} @endif</a></li>
	                                      <li class="d-inline pr-4"><i class="fa fa-bolt" aria-hidden="true"></i> {{$product->booked_number}} @lang('product/destination.booked')</li>
	                                      <li class="d-inline pr-5">                   
	                                          <span class=""><i class="text-warning fa fa-star"></i></span>
	                                            <span class=""><i class="text-warning fa fa-star"></i></span>
	                                          <span class=""><i class="text-warning fa fa-star"></i></span>
	                                          <span class=""><i class="text-warning fa fa-star"></i></span>
	                                           <span class=""><i class="text-warning fa fa-star"></i></span>
	                                            (2510) 
	                                     </li>
	                                      <li class="d-inline"><i class="fa fa-calendar" aria-hidden="true"></i> @lang('product/destination.Earliest Booking Date :') {{ \carbon\carbon::parse($product->date_time)->format('Y-m-d') }}</li>
	                                  </ul>
	                                  <div class="tress-inner mt-3">
	                                    <div class="tress-inner-1">
	                                      <a href="#" class="btn btn-default">@lang('product/destination.Instant Confirmation')</a>
	                                      <a href="#" class="btn btn-default">@lang('product/destination.Free cancellation')</a>
	                                    </div>
	                                    <div class="tress-inner-2">
	                                     <h5 class="text-right">HKD <span class="text-red">{{ $product->price }}</span></h5>
	                                    </div>
	                                  </div>

	                                </div>
	                              </div>
	                              </div>                           
	                            </div>
	                            @endif
	                            <?php $count++; ?>
                            @empty
                            <span class="alert alert-gray">No data available !</span>
                            @endforelse
                        </div>
                          <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                             <div class="row">
                              <div class="col-md-4">
                                <a href="#"><img src="{{ asset('theme1/images/recent.jpg') }}" alt=""></a>
                              </div>
                              <div class="col-md-8">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">12-01-2020 /</li>
                                      <li class="d-inline">Author</li>
                                  </ul>
                                  <h2>Why do we use it</h2>
                                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                </div>
                              </div>
                               <div class="col-md-4 mt-4">
                               <a href="#"><img src="{{ asset('theme1/images/recent.jpg') }}" alt=""></a>
                              </div>
                              <div class="col-md-8 mt-4">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">12-01-2020 /</li>
                                      <li class="d-inline">Author</li>
                                  </ul>
                                  <h2><a href="#">Why do we use it</a></h2>
                                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                </div>
                              </div>

                               <div class="col-md-4 mt-4">
                               <a href="#"><img src="{{ asset('theme1/images/recent.jpg') }}" alt=""></a>
                              </div>
                              <div class="col-md-8 mt-4">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">12-01-2020 /</li>
                                      <li class="d-inline">Author</li>
                                  </ul>
                                  <h2><a href="#">Why do we use it</a></h2>
                                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                             <div class="row">
                              <div class="col-md-4">
                                <a href="#"><img src="{{ asset('theme1/images/recent.jpg') }}" alt=""></a>
                              </div>
                              <div class="col-md-8">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">12-01-2020 /</li>
                                      <li class="d-inline">Author</li>
                                  </ul>
                                  <h2>Why do we use it</h2>
                                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                </div>
                              </div>

                               <div class="col-md-4 mt-4">
                               <a href="#"><img src="{{ asset('theme1/images/recent.jpg') }}" alt=""></a>
                              </div>
                              <div class="col-md-8 mt-4">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">12-01-2020 /</li>
                                      <li class="d-inline">Author</li>
                                  </ul>
                                  <h2><a href="#">Why do we use it</a></h2>
                                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                </div>
                              </div>
                               <div class="col-md-4 mt-4">
                               <a href="#"><img src="{{ asset('theme1/images/recent.jpg') }}" alt=""></a>
                              </div>
                              <div class="col-md-8 mt-4">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">12-01-2020 /</li>
                                      <li class="d-inline">Author</li>
                                  </ul>
                                  <h2><a href="#">Why do we use it</a></h2>
                                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                </div>
                              </div>
                               <div class="col-md-4 mt-4">
                               <a href="#"><img src="{{ asset('theme1/images/recent.jpg') }}" alt=""></a>
                              </div>
                              <div class="col-md-8 mt-4">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">12-01-2020 /</li>
                                      <li class="d-inline">Author</li>
                                  </ul>
                                  <h2><a href="#">Why do we use it</a></h2>
                                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                </div>
                              </div>
                               <div class="col-md-4 mt-4">
                               <a href="#"><img src="{{ asset('theme1/images/recent.jpg') }}" alt=""></a>
                              </div>
                              <div class="col-md-8 mt-4">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">12-01-2020 /</li>
                                      <li class="d-inline">Author</li>
                                  </ul>
                                  <h2><a href="#">Why do we use it</a></h2>
                                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                

                 </div>

            </div>
                     
            </div>
             
        </div>
   
    </div>
   
</section>

<!-- service-section end -->

<!-- Footer start -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="news-text">
                    <p>Subscribe to RTG's newsletter to get the latest promos, exclusive discounts and helpful travel tips!</p>
                    <p>You agree to our<a href="#"> Terms & Conditions</a> and <a href="#">Privacy Policy</a> when you click Subscribe</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <form class="subscribe_form">
                    <div class="input-group">
                       <input type="text" class="form-control" name="email" placeholder="Please enter your email address!">
                       <span class="input-group-btn">
                            <button class="btn btn-default" type="button">subscribe</button>
                       </span>
                    </div>
                </form>
            </div>
        </div>
         <div class="border-tb"> 
        <div class="row">                 
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">About Us</a></li>
                         <li><a href="#">Terms & Conditions</a></li>
                          <li><a href="#">Privacy Policy</a></li>
                           <li><a href="#">FAQ</a></li>
                            <li><a href="#">Press</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Resources</h5>
                    <ul class="f-li">
                        <li><a href="#">Why RTG?</a></li>
                         <li><a href="#">Blog</a></li>
                          <li><a href="#">RTG Points</a></li>
                           <li><a href="#">Promotions</a></li>
                            <li><a href="#">Student Store</a></li>
                    </ul>
                </div>
                  <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Work with RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">Become a Supplier</a></li>
                         <li><a href="#">Distributor Collaboration</a></li>
                          <li><a href="#">Affiliate Program</a></li>
                           <li><a href="#">Careers</a></li>
                            <li><a href="#">Influencer Partnerships</a></li>
                             <li><a href="#">Selected Promotions</a></li>
                    </ul>
                </div>
                   <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Contact Us</h5>
                    <ul class="f-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
                </div>
        </div>
        </div> 
        <p class="copy-text">COPYRIGHT © 2020 RTG All rights reserved</p>
    </div>
</footer>


<!-- Footer end -->




	<script src="{{ asset('theme1/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('theme1/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('theme1/js/custom.js') }}"></script>
    <script>
        $(function() {
            $( "#slider-range" ).slider({
              range: true,
              min: 1,
              max: 14,
              values: [ 1, 14 ],
              slide: function( event, ui ) {
              $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
              }
            });
            $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
              " - $" + $( "#slider-range" ).slider( "values", 1 ) );
          });
    </script>

    <script type="text/javascript">
    	$('#language_all').change(function(){

    		 $(".language").prop('checked', $(this).prop("checked"));

    	});

    	$('.language').change(function(){
    		 language();
    	});

    	$(document).ready(function(){
    		// alert($('.language').length);
    		// alert($('.language :checked').length);
    	});

    	var language = function()
    	{
    		var total_check_lang = $('.language:checked').length;
    		var total_lang = $('.language').length;

    		if(total_check_lang == total_lang)
    		{
				$("#language_all").prop("checked", true);
    			var lang = 'all';
    		}
    		else
    		{
    			$("#language_all").prop("checked", false);
    			var lang = $('.language').val();
    		}

    		var url = "{{ url('filter_destination') }}";
    		$.ajax({
    			type: 'POST',
    			url: url,
    			data: {
    				"lang": lang
    			},
    			success: function(data)
    			{
    				$('#filter_product').html(data);
    			}
    		});
    	}
    </script>

</body>
</html>
