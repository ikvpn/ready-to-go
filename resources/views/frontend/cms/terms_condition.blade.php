<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Term & Condition</title>
	  <link href="{{ asset('theme1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/responsive.css') }}" rel="stylesheet" type="text/css">
	  <link href="{{ asset('theme1/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/slick.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/slick-theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">   
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
 <!--    <link rel="stylesheet" type="text/css" href="css/style.css"> -->
    
</head>
<body>
<!-- header start -->    

<header class="sticky-top">
<nav class="navbar navbar-expand-lg navbar-light">
     <div class="container">
  <a class="navbar-brand" href="#"><img src="{{ asset('theme1/images/logo.png') }}" alt=""></a></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> 
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Parnter</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">Newsroom</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">FAQ</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        English
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Chines</a>
          <a class="dropdown-item" href="#"></a>
        </div>
      </li>     
    </ul>    
  </div>
</div>
</nav>
</header>

<!-- header end -->

<!-- hero-section-1 start -->

<section class="about-part">
<div class="container">       
            <div class="row">                
                <div class="col-lg-6 col-md-12 col-sm-12">
                      <div class="hero-part-1">
                      <h1 class="t-title">@lang('cms/terms_condition.Terms and Conditions')</h1>                                       
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12"></div>
            </div>
</div>
</section>

<!-- hero-section-1 end -->

<!-- service-section srart -->
<section class="t-c-part ptg-50">
    <div class="container"> 
        <div class="row">           
              <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="inner-tc">
                  <div class="row">
                    <div class="col-lg-9"></div>
                    <div class="col-lg-3">
                        <div class="verson-part">
                          @if(isset($terms->effective_date)) <h6>@lang('cms/terms_condition.Effective Date :') {{ $terms->effective_date }}</h6> @endif
                          @if(isset($terms->version)) <h6>@lang('cms/terms_condition.Version :') 1.1.1</h6> @endif
                        </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="tc-inner pt-3">
                        <h2>@if(app()->getLocale() == 'en') {{ $terms->title }} @else {{ $terms->title_zh }} @endif</h2>
                        @if(app()->getLocale() == 'en') {!! $terms-> discription !!} @else {!! $terms->discription_zh !!} @endif
                      </div>
                    </div>
                  </div>
                </div>
            </div>  

        </div>
    </div>
</section>


<!-- Footer start -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="news-text">
                    <p>Subscribe to RTG's newsletter to get the latest promos, exclusive discounts and helpful travel tips!</p>
                    <p>You agree to our<a href="#"> Terms & Conditions</a> and <a href="#">Privacy Policy</a> when you click Subscribe</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <form class="subscribe_form">
                    <div class="input-group">
                       <input type="text" class="form-control" name="email" placeholder="Please enter your email address!">
                       <span class="input-group-btn">
                            <button class="btn btn-default" type="button">subscribe</button>
                       </span>
                    </div>
                </form>
            </div>
        </div>
         <div class="border-tb"> 
        <div class="row">                 
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">About Us</a></li>
                         <li><a href="#">Terms & Conditions</a></li>
                          <li><a href="#">Privacy Policy</a></li>
                           <li><a href="#">FAQ</a></li>
                            <li><a href="#">Press</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Resources</h5>
                    <ul class="f-li">
                        <li><a href="#">Why RTG?</a></li>
                         <li><a href="#">Blog</a></li>
                          <li><a href="#">RTG Points</a></li>
                           <li><a href="#">Promotions</a></li>
                            <li><a href="#">Student Store</a></li>
                    </ul>
                </div>
                  <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Work with RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">Become a Supplier</a></li>
                         <li><a href="#">Distributor Collaboration</a></li>
                          <li><a href="#">Affiliate Program</a></li>
                           <li><a href="#">Careers</a></li>
                            <li><a href="#">Influencer Partnerships</a></li>
                             <li><a href="#">Selected Promotions</a></li>
                    </ul>
                </div>
                   <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Contact Us</h5>
                    <ul class="f-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
                </div>
        </div>
        </div> 
        <p class="copy-text">COPYRIGHT © 2020 RTG All rights reserved</p>
    </div>
</footer>
<!-- Footer end -->

	<script src="{{ asset('theme1/js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('theme1/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('theme1/js/custom.js') }}"></script>
  <script src="{{ asset('theme1/js/slick.min.js') }}"></script>


</body>
</html>
