<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Press</title>
	<link href="{{ asset('theme1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/responsive.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('theme1/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/slick.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/slick-theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">   
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
 <!--    <link rel="stylesheet" type="text/css" href="css/style.css"> -->
    
</head>
<body>
<!-- header start -->    

<header class="sticky-top">
<nav class="navbar navbar-expand-lg navbar-light">
     <div class="container">
  <a class="navbar-brand" href="#"><img src="{{ asset('theme1/images/logo.png') }}" alt=""></a></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> 
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Parnter</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">Newsroom</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">FAQ</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        English
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Chines</a>
          <a class="dropdown-item" href="#"></a>
        </div>
      </li>     
    </ul>    
  </div>
</div>
</nav>
</header>

<!-- header end -->

<!-- hero-section-1 start -->

<section class="about-part">
<div class="container">       
            <div class="row">                
                <div class="col-lg-6 col-md-12 col-sm-12">
                      <div class="hero-part-1">
                      <h2 class="t-title">@lang('cms/press.Press')</h2>                                       
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12"></div>
            </div>
</div>
</section>

<!-- hero-section-1 end -->

<!-- service-section srart -->
<section class="t-c-part ptg-50">
    <div class="container"> 
        <div class="row">           
              <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="inner-tc">
                  <div class="row">                    
                    <div class="col-lg-12">
                      <div class="">
                       <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">@lang('cms/press.Business')</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">@lang('cms/press.Event')</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">@lang('cms/press.Fundings')</a>
                          </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="row">
                              @forelse($businesses as $business)
                              <?php $count = 0; ?>
                              	<div class="col-md-4 @if($count == 0) mt-4 @endif">
                                <a href="#"><img src="{{ asset('theme1/images/'.$business->image) }}" alt=""></a>
                              </div>	
                              <div class="col-md-8 @if($count == 0) mt-4 @endif">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">{{ \Carbon\Carbon::parse($business->date)->format('d-m-Y') }}/</li>
                                      <li class="d-inline">@if(app()->getLocale() == 'en') {{ $business->author }} @else {{ $business->author_zh }} @endif</li>
                                  </ul>
                                  <h2>@if(app()->getLocale() == 'en') {{ $business->title }} @else {{ $business->title_zh }} @endif</h2>
                                  <p>@if(app()->getLocale() == 'en') {{ $business->discription }} @else {{ $business->discription_zh }} @endif</p>
                                </div>
                              </div>
                              <?php $count++; ?>
                              @empty
                              	<div class="alert alert-secondary">Business data is not available !</div>
                              @endforelse
                          	</div>
                        </div>
                          <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                              <div class="row">
                              @forelse($events as $event)
                              <?php $count = 0; ?>
                              	<div class="col-md-4 @if($count == 0) mt-4 @endif">
                                <a href="#"><img src="{{ asset('theme1/images/'.$event->image) }}"" alt=""></a>
                              </div>	
                              <div class="col-md-8 @if($count == 0) mt-4 @endif">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">{{ \Carbon\Carbon::parse($event->date)->format('d-m-Y') }}/</li>
                                      <li class="d-inline">@if(app()->getLocale() == 'en') {{ $event->author }} @else {{ $event->author_zh }} @endif</li>
                                  </ul>
                                  <h2>@if(app()->getLocale() == 'en') {{ $event->title }} @else {{ $event->title_zh }} @endif</h2>
                                  <p>@if(app()->getLocale() == 'en') {{ $event->discription }} @else {{ $event->discription_zh }} @endif</p>
                                </div>
                              </div>
                              <?php $count++; ?>
                              @empty
                              	<div class="alert alert-secondary">Business data is not available !</div>
                              @endforelse
                          	</div>
                          </div>
                          <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                             <div class="row">
                              @forelse($fundings as $funding)
                              <?php $count = 0; ?>
                              	<div class="col-md-4 @if($count == 0) mt-4 @endif">
                                <a href="#"><img src="{{ asset('theme1/images/'.$funding->image) }}" alt=""></a>
                              </div>	
                              <div class="col-md-8 @if($count == 0) mt-4 @endif">
                                <div class="press-text">
                                  <ul>
                                      <li class="d-inline">{{ \Carbon\Carbon::parse($funding->date)->format('d-m-Y') }}/</li>
                                      <li class="d-inline">@if(app()->getLocale() == 'en') {{ $funding->author }} @else {{ $funding->author_zh }} @endif</li>
                                  </ul>
                                  <h2>@if(app()->getLocale() == 'en') {{ $funding->title }} @else {{ $funding->title_zh }} @endif</h2>
                                  <p>@if(app()->getLocale() == 'en') {{ $funding->discription }} @else {{ $funding->discription_zh }} @endif</p>
                                </div>
                              </div>
                              <?php $count++; ?>
                              @empty
                              	<div class="alert alert-secondary">Business data is not available !</div>
                              @endforelse
                          	</div>
                          </div>
                        </div>
                      </div>
                    </div> 
                  </div>
                      

                </div>
            </div>  

        </div>
    </div>
</section>


<!-- Footer start -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="news-text">
                    <p>Subscribe to RTG's newsletter to get the latest promos, exclusive discounts and helpful travel tips!</p>
                    <p>You agree to our<a href="#"> Terms & Conditions</a> and <a href="#">Privacy Policy</a> when you click Subscribe</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <form class="subscribe_form">
                    <div class="input-group">
                       <input type="text" class="form-control" name="email" placeholder="Please enter your email address!">
                       <span class="input-group-btn">
                            <button class="btn btn-default" type="button">subscribe</button>
                       </span>
                    </div>
                </form>
            </div>
        </div>
         <div class="border-tb"> 
        <div class="row">                 
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">About Us</a></li>
                         <li><a href="#">Terms & Conditions</a></li>
                          <li><a href="#">Privacy Policy</a></li>
                           <li><a href="#">FAQ</a></li>
                            <li><a href="#">Press</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Resources</h5>
                    <ul class="f-li">
                        <li><a href="#">Why RTG?</a></li>
                         <li><a href="#">Blog</a></li>
                          <li><a href="#">RTG Points</a></li>
                           <li><a href="#">Promotions</a></li>
                            <li><a href="#">Student Store</a></li>
                    </ul>
                </div>
                  <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Work with RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">Become a Supplier</a></li>
                         <li><a href="#">Distributor Collaboration</a></li>
                          <li><a href="#">Affiliate Program</a></li>
                           <li><a href="#">Careers</a></li>
                            <li><a href="#">Influencer Partnerships</a></li>
                             <li><a href="#">Selected Promotions</a></li>
                    </ul>
                </div>
                   <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Contact Us</h5>
                    <ul class="f-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
                </div>
        </div>
        </div> 
        <p class="copy-text">COPYRIGHT © 2020 RTG All rights reserved</p>
    </div>
</footer>
<!-- Footer end -->

	<script src="{{ asset('theme1/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('theme1/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('theme1/js/custom.js') }}"></script>
    <script src="{{ asset('theme1/js/slick.min.js') }}"></script>


</body>
</html>
