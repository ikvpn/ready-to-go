<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ready to go</title>
	<link href="{{ asset('theme1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('theme1/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/responsive.css') }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
</head>
<body>


<!-- header start -->    
<div class="top-container">
  <div class="container">
    <ul class="t-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
    <div class="title-section">
      <div class="row">
        <div class="col-lg-3 col-sm-12"></div>
        <div class="col-lg-6 col-sm-12">
      <h1>@if(app()->getLocale() == 'en') {{ strtoupper($slider->title) }} @else {{ $slider->title_zh }} @endif</h1>
      <p>@if(app()->getLocale() == 'en') {{ $slider->discription }} @else {{ $slider->discription_zh }} @endif</p>
        </div>
        <div class="col-lg-3 col-sm-12"></div>
      </div>
    </div>
    <a href="#myHeader"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
  </div>
</div>

<div class="header" id="myHeader">
 <div class="container">
  <div class="row">
  <div class="col-lg-1"></div>
  <div class="col-lg-10 text-center">
   <nav class="navbar navbar-expand-lg navbar-light d-inline-block">     
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button> 
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        @foreach($navbar as $nav)
        <li class="nav-item {{ Request::segment(2) === $nav->country ? 'active' : null }}">
          <a class="nav-link" href="{{ url('travel_country',$nav->country) }}">@if(app()->getLocale() == 'en') {{ strtoupper($nav->country) }} @else {{ $nav->country_zh }} @endif</a>
        </li>
        @endforeach
      </ul>    
    </div>
 
  </nav>
</div>
<div class="col-lg-1"></div>
</div>
 </div>
</div>

<!-- header end -->

<!-- US  srart -->
<section class="us-section ptg-50">
   <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-12">
        <div class="us-inner">
          <div class="row">
          	<?php $count = 0; ?>
          	@foreach($posts as $post)
	          	@if($count == 0)
		            <div class="col-lg-12">
		              <div class="us-blogs">
                    <img src="{{asset('theme1/images/'.$post->image)}}">
			              <ul class="us-catalog">
			              	<?php $count_tab = 0; ?>
			              	@foreach($post->all_categories['all_categories'] as $all_category)
			              		@if(count($post->all_categories['all_categories']) != $count_tab)
					                <li class="d-inline pr-2"><a href="{{ url('travel_category',$all_category->url) }}" class="btn btn-primary">@if(app()->getLocale() == 'en') {{ $all_category->category }} @else {{ $all_category->category_zh }} @endif</a></li>
				               	@else
                          <li class="d-inline"><a href="{{ url('travel_category',$all_category->url) }}" class="btn btn-primary">@if(app()->getLocale() == 'en') {{ $all_category->category }} @else {{ $all_category->category_zh }} @endif</a></li>
                        @endif
				               	<?php $count_tab++; ?>
			                @endforeach
			              </ul>
			              <div class="us-text">
			                <span>@lang('cms/travel_country.DESTINATIONS')</span>
			                <a href="#">
			                  <h4>@if(app()->getLocale() == 'en') {{ $post->title }}  @else {{ $post->title_zh }} @endif</h4>
			                </a>
			              </div>
		              </div>
		            </div>  
	            @else
		            <div class="col-lg-6 mt-4">
		              <div class="small-us">
			              <div class="us-blogs">
                    <img src="{{asset('theme1/images/'.$post->image)}}">
				              <ul class="us-catalog">
				                <?php $count_tab = 1; ?>
				              	@foreach($post->all_categories['all_categories'] as $all_category)
				              		@if(count($post->all_categories['all_categories']) != $count_tab)
						                <li class="d-inline pr-2"><a href="{{ url('travel_category',$all_category->url) }}" class="btn btn-primary">@if(app()->getLocale() == 'en') {{ $all_category->category }} @else {{ $all_category->category_zh }} @endif</a></li>
					               	@else
                            <li class="d-inline"><a href="{{ url('travel_category',$all_category->url) }}" class="btn btn-primary">@if(app()->getLocale() == 'en') {{ $all_category->category }} @else {{ $all_category->category_zh }} @endif</a></li>
                          @endif
					               	<?php $count_tab++; ?>
				                @endforeach
				              </ul>
				              <div class="us-text">
				                <span>@lang('cms/travel_country.DESTINATIONS')</span>
                        <a href="#">
                          <h4>@if(app()->getLocale() == 'en') {{ $post->title }}  @else {{ $post->title_zh }} @endif</h4>
                        </a>
				              </div>
			              </div>
			          </div>
		            </div>
	            @endif
	            <?php $count++; ?>
            @endforeach
          </div>
        </div>
           <nav aria-label="...">
              <ul class="pagination pagination-section float-right mt-4">   
                <!-- <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item ">
                  <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">5</a></li>
                <li class="page-item"><a class="page-link" href="#">6</a></li>     -->
                {!! $posts->links() !!}
              </ul>
            </nav>
      </div>      
        <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="sidebar-part">
          <div class="col-lg-12">
            <div class="search-part">
              <form>
                <div class="form-group">
                  <label for="textpart"><strong>@lang('cms/travel_country.From')</strong></label>
                  <input type="text" class="form-control" id="textpart" aria-describedby="" placeholder="">                  
                </div>
                 <div class="form-group">
                  <label for="textpart"><strong>@lang('cms/travel_country.To')</strong></label>
                  <input type="text" class="form-control" id="textpart" aria-describedby="" placeholder="">                  
                </div>
                <ul>
                  <li class="d-inline pr-5"><label class="check-parts">@lang('cms/travel_country.Return')
                      <input type="checkbox">
                      <span class="checkmark"></span>
                    </label>
                  </li>
                   <li class="d-inline"><label class="check-parts">@lang('cms/travel_country.One way')
                      <input type="checkbox">
                      <span class="checkmark"></span>
                    </label>
                  </li>
                </ul>
                <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group">
                      <label for="textpart"><strong>@lang('cms/travel_country.Depart')</strong></label>
                      <input type="date" class="form-control" id="textpart" aria-describedby="" placeholder="">                  
                       </div>
                  </div>
                   <div class="col-lg-6">
                     <div class="form-group">
                      <label for="textpart"><strong>@lang('cms/travel_country.Return')</strong></label>
                      <input type="date" class="form-control" id="textpart" aria-describedby="" placeholder="">                  
                       </div>
                  </div>
                  
                </div>
          
                <a href="#" class="btn btn-default">@lang('cms/travel_country.Search flghts')</a>
              </form>
            </div>
          </div>
           <div class="col-lg-12 mt-5">
            <div class="search-part">
              <h4 class="text-center mb-5">@lang('cms/travel_country.RECENT POSTS')</h4>
              <?php $count = 0; ?>
              @foreach($recent_posts as $post)
	              @if($count == 0)
		              <div class="media ">
		               <a href="#"> <img class="align-self-center mr-3" src="{{ asset('theme1/images/'.$post->image) }}" alt="Generic placeholder image"></a>
		                <div class="media-body">
		                  <h5 class="mt-0"><a href="#">@if(app()->getlocale() == 'en') {{ $post->title }} @else {{ $post->title_zh }} @endif</a></h5>
		                  <p>{{ $post->date }}</p>                  
		                </div>
		              </div>
	              @else
		              <div class="media mt-4">
		               <a href="#"> <img class="align-self-center mr-3" src="{{ asset('theme1/images/'.$post->image) }}" alt="Generic placeholder image"></a>
		                <div class="media-body">
		                  <h5 class="mt-0"><a href="#">@if(app()->getlocale() == 'en') {{ $post->title }} @else {{ $post->title_zh }} @endif</a></h5>
		                  <p>2020-05-20</p>                  
		                </div>
		              </div>
	              @endif
	              <?php $count++; ?>
              @endforeach
            </div>
          </div>

                <div class="col-lg-12 mt-5">
            <div class="search-part text-center">
              <h4 class="text-center mb-5">@lang('cms/travel_country.ABOUT US')</h4>
             <p>
              @lang('cms/travel_country.Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem')
             </p>
             <a href="{{ url('/') }}" class="btn btn-default mt-5">@lang('cms/travel_country.Visit RTG')</a>            
            </div>
          </div>

        </div>
      </div>
    </div>
   
  
  
</div>  
</section>

<!-- US-section end -->



<!-- Footer start -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="top-container">
                   <ul class="t-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="copy-part">
                <p class="copy-text">COPYRIGHT © 2020 RTG All rights reserved</p>
              </div>
            </div>
        </div>       
        
    </div>
</footer>


<!-- Footer end -->



  
	<script src="{{ asset('theme1/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('theme1/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('theme1/js/custom.js') }}"></script>

</body>
</html>
