<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>header</title>
	<link href="{{ asset('theme1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
   	<link href="{{ asset('theme1/css/responsive.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('theme1/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/slick.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/slick-theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">   
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
 <!--    <link rel="stylesheet" type="text/css" href="css/style.css"> -->
    
</head>
<body>
<!-- header start -->    

<header class="sticky-top">
<nav class="navbar navbar-expand-lg navbar-light">
     <div class="container">
  <a class="navbar-brand" href="#"><img src="{{ asset('theme1/images/logo.png') }}" alt=""></a></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> 
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Parnter</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">Newsroom</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">FAQ</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        English
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Chines</a>
          <a class="dropdown-item" href="#"></a>
        </div>
      </li>     
    </ul>    
  </div>
</div>
</nav>
</header>

<!-- header end -->

<!-- hero-section-1 start -->

<section class="about-part">
<div class="container"> 
      
            <div class="row">                
                <div class="col-lg-6 col-md-6 col-sm-12">
                      <div class="hero-part-1">
                      <h1>@lang('cms/aboutus.About Us')</h1>  
                      <p class="mt-3">@lang("cms/aboutus.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard") </p>                  
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12"></div>
            </div>
</div>
</section>

<!-- hero-section-1 end -->

<!-- service-section srart -->
<section class="founder-part">
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="inner-part-1">
                                     
                </div>
            </div>
              <div class="col-lg-8 col-md-8 col-sm-12">
                <div class="inner-part">
                    <h2>@lang('cms/aboutus.Founder')</h2> 
                    <h3>@lang("cms/aboutus.Founder's name")</h3>
                    <P>@lang("cms/aboutus.Founder's info")</P>
                </div>
            </div>  

        </div>
    </div>
</section>

<!-- service-section end -->
<section class="Experince-part">
    <div class="container"> 
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
               <div class="media">
                <img class="align-self-start mr-3" src="{{ asset('theme1/images/exp-1.png') }}" alt="Generic placeholder image">
                <div class="media-body align-self-end">
                  <h5 class="mt-0 text-white">@lang('cms/aboutus.50+ years experience')</h5>    
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12">
               <div class="media">
                <img class="align-self-start mr-3" src="{{ asset('theme1/images/exp-2.png') }}" alt="Generic placeholder image">
                <div class="media-body align-self-end">
                  <h5 class="mt-0 text-white">@lang('cms/aboutus.100+ Local Tour')</h5>    
                </div>
              </div>
            </div>  

             <div class="col-lg-4 col-md-4 col-sm-12">
                 <div class="media">
                <img class="align-self-start mr-3" src="{{ asset('theme1/images/exp-3.png') }}" alt="Generic placeholder image">
                <div class="media-body align-self-end">
                  <h5 class="mt-0 text-white">@lang('cms/aboutus.500+ Cities')</h5>    
                </div>
              </div>
            </div> 
            
        </div>
    </div>
</section>

<section class="about-part">
<div class="container"> 
      
            <div class="row">                
                <div class="col-lg-6 col-md-6 col-sm-12">
                      <div class="hero-part-1">
                      <h1>@lang('cms/aboutus.Location')</h1>  
                      <h3 class="mt-3">@lang('cms/aboutus.HONG KONG')</h3>                  
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12"></div>
            </div>
</div>
</section>

<!-- Footer start -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="news-text">
                    <p>Subscribe to RTG's newsletter to get the latest promos, exclusive discounts and helpful travel tips!</p>
                    <p>You agree to our<a href="#"> Terms & Conditions</a> and <a href="#">Privacy Policy</a> when you click Subscribe</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <form class="subscribe_form">
                    <div class="input-group">
                       <input type="text" class="form-control" name="email" placeholder="Please enter your email address!">
                       <span class="input-group-btn">
                            <button class="btn btn-default" type="button">subscribe</button>
                       </span>
                    </div>
                </form>
            </div>
        </div>
         <div class="border-tb"> 
        <div class="row">                 
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">About Us</a></li>
                         <li><a href="#">Terms & Conditions</a></li>
                          <li><a href="#">Privacy Policy</a></li>
                           <li><a href="#">FAQ</a></li>
                            <li><a href="#">Press</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Resources</h5>
                    <ul class="f-li">
                        <li><a href="#">Why RTG?</a></li>
                         <li><a href="#">Blog</a></li>
                          <li><a href="#">RTG Points</a></li>
                           <li><a href="#">Promotions</a></li>
                            <li><a href="#">Student Store</a></li>
                    </ul>
                </div>
                  <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Work with RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">Become a Supplier</a></li>
                         <li><a href="#">Distributor Collaboration</a></li>
                          <li><a href="#">Affiliate Program</a></li>
                           <li><a href="#">Careers</a></li>
                            <li><a href="#">Influencer Partnerships</a></li>
                             <li><a href="#">Selected Promotions</a></li>
                    </ul>
                </div>
                   <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Contact Us</h5>
                    <ul class="f-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
                </div>
        </div>
        </div> 
        <p class="copy-text">COPYRIGHT © 2020 RTG All rights reserved</p>
    </div>
</footer>
<!-- Footer end -->

	<script src="{{ asset('theme1/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('theme1/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('theme1/js/custom.js') }}"></script>
    <script src="{{ asset('theme1/js/slick.min.js') }}"></script>


</body>
</html>
