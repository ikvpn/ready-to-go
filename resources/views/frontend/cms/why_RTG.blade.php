<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ready to go</title>
    <link href="{{ asset('theme1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/responsive.css') }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
    
</head>
<body>


<!-- header start -->    

<header class="sticky-top">
<nav class="navbar navbar-expand-lg navbar-light">
     <div class="container">
  <a class="navbar-brand" href="#"><img src="images/logo.png" alt=""></a></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> 
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Parnter</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">Newsroom</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">FAQ</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        English
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Chines</a>
          <a class="dropdown-item" href="#"></a>
        </div>
      </li>     
    </ul>    
  </div>
</div>
</nav>
</header>

<!-- header end -->

<!-- why RTG start -->

<section class="why-part">
<div class="container">       
            <div class="row">                
                <div class="col-lg-6 col-sm-6">
                      <div class="rtg-part">
                      <h4>@lang('cms/why_RTG.Why RTG ?')</h4>
                     <p>@lang('cms/why_RTG.See why millions of travelers choose to experience the world as part of our strong and secure RTG community.')</p>
                  </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                  <div class="image-part">
                    <img src="{{ asset('theme1/images/why-rtg.png') }}" alt="">
                  </div>
                </div>               
            </div>
</div>
</section>

<!-- hero-section-1 end -->

<!-- service-section srart -->
<section class="why-service">
    <div class="container"> 
        <div class="row">
          @foreach($communities as $community)
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="icon-inner">
                    <img src="{{ asset('theme1/images/cms/why_RTG/'.$community->image) }}" class="pb-3" alt="icon">
                    <h5>@if(app()->getLocale() == 'en') {{ $community->title }} @else {{ $community->title_zh }} @endif</h5>
                    <p>@if(app()->getLocale() == 'en') {{ $community->discription }} @else {{ $community->discription_zh }} @endif</p>
                </div>
            </div>
          @endforeach  
        </div>
        <div class="row">
          <div class="col-lg-12 col-sm-12">
          <h2 class="title">@lang('cms/why_RTG.A secure platform accessible from multiple devices')</h2>
          </div>
          <div class="col-lg-4 col-sm-6">
            <div class="box-inner">                   
                    <h5>@lang('cms/why_RTG.Safety and Encryption')</h5>
                    <P>@lang('cms/why_RTG.Safety and security is of utmost importance to us. Your personal information along with payment methods are kept highly encrypted and will never be shared with a third party.')</P>
                </div>
          </div>
          <div class="col-lg-4 col-sm-6">
            <div class="box-inner">                   
                    <h5>@lang('cms/why_RTG.Seamless and Secure')</h5>
                    <P>@lang("cms/why_RTG.Travelers pay through KKday when they make a booking. After the tour or experience is over the service provider will receive payment directly from KKday. We'll manage the transactions while you go out and have fun.")</P>
                </div>
          </div>
          <div class="col-lg-4 col-sm-6">
            <div class="box-inner">                   
                    <h5>@lang('cms/why_RTG.Browse and book anywhere')</h5>
                    <P>@lang("cms/why_RTG.It doesn't matter if you like to plan ahead or if you are the spontaneous type. Have the freedom to make secure bookings on the go and get instant confirmations.")</P>
                </div>
          </div>
        </div>
    </div>
     <div class="bg-part">
    <div class="container-fluid">     
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
           <h2 class="title">@lang('cms/why_RTG.Experience the freedom to discover and book what you never knew was possible !')</h2>
              <p>@lang("cms/why_RTG.We've done the hard work so you can access the best your destination has to offer. Don't just take the road less taken- determine your own journey with us.")</p>
              <a href="#" class="btn btn-default">@lang('cms/why_RTG.Explore NOW !')</a>
        </div>
        <div class="col-lg-3"></div>
      </div>
      </div>
    </div>
</section>

<!-- service-section end -->

<!-- Footer start -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="news-text">
                    <p>Subscribe to RTG's newsletter to get the latest promos, exclusive discounts and helpful travel tips!</p>
                    <p>You agree to our<a href="#"> Terms & Conditions</a> and <a href="#">Privacy Policy</a> when you click Subscribe</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <form class="subscribe_form">
                    <div class="input-group">
                       <input type="text" class="form-control" name="email" placeholder="Please enter your email address!">
                       <span class="input-group-btn">
                            <button class="btn btn-default" type="button">subscribe</button>
                       </span>
                    </div>
                </form>
            </div>
        </div>
         <div class="border-tb"> 
        <div class="row">                 
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">About Us</a></li>
                         <li><a href="#">Terms & Conditions</a></li>
                          <li><a href="#">Privacy Policy</a></li>
                           <li><a href="#">FAQ</a></li>
                            <li><a href="#">Press</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Resources</h5>
                    <ul class="f-li">
                        <li><a href="#">Why RTG?</a></li>
                         <li><a href="#">Blog</a></li>
                          <li><a href="#">RTG Points</a></li>
                           <li><a href="#">Promotions</a></li>
                            <li><a href="#">Student Store</a></li>
                    </ul>
                </div>
                  <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Work with RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">Become a Supplier</a></li>
                         <li><a href="#">Distributor Collaboration</a></li>
                          <li><a href="#">Affiliate Program</a></li>
                           <li><a href="#">Careers</a></li>
                            <li><a href="#">Influencer Partnerships</a></li>
                             <li><a href="#">Selected Promotions</a></li>
                    </ul>
                </div>
                   <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Contact Us</h5>
                    <ul class="f-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
                </div>
        </div>
        </div> 
        <p class="copy-text">COPYRIGHT © 2020 RTG All rights reserved</p>
    </div>
</footer>


<!-- Footer end -->




	<script src="{{ asset('theme1/js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('theme1/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('theme1/js/custom.js') }}"></script>

</body>
</html>
