<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ready to go</title>
	<link href="{{ asset('theme1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('theme1/css/style.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('theme1/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('theme1/css/responsive.css') }}" rel="stylesheet" type="text/css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
</head>
<body>


<!-- header start -->    

<header class="sticky-top">
<nav class="navbar navbar-expand-lg navbar-light">
     <div class="container">
  <a class="navbar-brand" href="#"><img src="{{ asset('theme1/images/logo.png') }}" alt=""></a></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> 
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Redeem Rewards</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">What's RTG Points?</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">My Record</a>
      </li>      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        English
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Chines</a>
          <a class="dropdown-item" href="#"></a>
        </div>
      </li>     
    </ul>    
  </div>
</div>
</nav>
</header>

<!-- header end -->

<!-- why RTG start -->

<section class="part-rtg">
<div class="container">       
            <div class="row">                
                <div class="col-lg-10 col-sm-6">
                      <div class="rtg-part">
                      <h4>@lang('cms/promotion.RTG Points') </h4>
                      <h4>@lang('cms/promotion.RTG Points Member Exclusive Offers')</h4>
                      </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                  <div class="image-part">
                    <img src="{{ asset('theme1/images/why-rtg-2.png') }}" alt="">
                  </div>
                </div>               
            </div>
</div>
</section>

<!-- hero-section-1 end -->

<!-- service-section srart -->
<section class="why-service">
    <div class="container"> 
      <div class="row">
        <div class="col-lg-12 col-sm-12">
          <h2 class="title">@lang('cms/promotion.Welcome Offer')</h2>
        </div>
        @foreach($welcome_offers as $welcome_offer)
        <div class="col-lg-4 col-md-6 col-sm-12">
          <div class="baloon-part">
             <h6 class="m-0">@if(app()->getLocale() == 'en') {{ $welcome_offer->title }} @else {{ $welcome_offer->title_zh }} @endif</h6>
             <p>@lang('cms/promotion.Earned points') {{$welcome_offer->points}} @lang('cms/promotion.points')</p>
             <a href="#" class="btn primary-line mt-4">@lang('cms/promotion.Redeem')</a>                
          </div>   
          <div class="text-birtdy">
             <p>@if(app()->getLocale() == 'en') {{ $welcome_offer->discription }} @else {{ $welcome_offer->discription_zh }} @endif</p>
          </div>
        </div>
        @endforeach
    </div>


     <div class="row mt-5 Poinst-part">
        <div class="col-lg-12 col-sm-12">
          <h2 class="title">@lang('cms/promotion.Your total RTG Points :')<span class="text-reds"> 200</span></h2>
        </div>
        @foreach($special_offers as $special_offer)
        <div class="col-lg-4 col-md-6 col-sm-12">
          <div class="baloon-part-2">
            <a href="#"> <h6 class="m-0">@if(app()->getLocale() == 'en') {{ $special_offer->title }} @else {{ $special_offer->title_zh }} @endif<br> HKD 20</h6></a>             
          </div>   
          <div class="text-birtdy">
            <ul>
              <li class="d-inline"><i class="fa fa-gift" aria-hidden="true"></i> @lang('cms/promotion.Gift quantity :') {{ $special_offer->quantity }}</li>
              <li class="d-inline pl-3"><i class="fa fa-product-hunt" aria-hidden="true"></i>{{ $special_offer->points }} @lang('cms/promotion.Points')</li>
            </ul>
          </div>
        </div>
        @endforeach
          
    </div>
  
        <div class="row">
            @foreach($points as $point)
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="icon-inner">
                    <img src="{{ asset('theme1/images/cms/RTG_point/'.$point->image) }}" class="pb-3" alt="icon">
                    <h5>@if(app()->getLocale() == 'en') {{ $point->title }} @else {{ $point->title_zh }} @endif</h5>
                    <p>@if(app()->getLocale() == 'en') {{ $point->discription }} @else {{ $point->discription_zh }} @endif</p>
                </div>
            </div>        
            @endforeach          
        </div>
        <a href="#" class="btn btn-default center-part mt-5">@lang('cms/promotion.More')</a>
</section>
<section class="download-part">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <h2>@lang('cms/promotion.Download our app !')</h2>
      </div>
      <div class="col-lg-6"></div>
    </div>
  </div>
</section>
<!-- service-section end -->



<!-- Footer start -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="news-text">
                    <p>Subscribe to RTG's newsletter to get the latest promos, exclusive discounts and helpful travel tips!</p>
                    <p>You agree to our<a href="#"> Terms & Conditions</a> and <a href="#">Privacy Policy</a> when you click Subscribe</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <form class="subscribe_form">
                    <div class="input-group">
                       <input type="text" class="form-control" name="email" placeholder="Please enter your email address!">
                       <span class="input-group-btn">
                            <button class="btn btn-default" type="button">subscribe</button>
                       </span>
                    </div>
                </form>
            </div>
        </div>
         <div class="border-tb"> 
        <div class="row">                 
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">About Us</a></li>
                         <li><a href="#">Terms & Conditions</a></li>
                          <li><a href="#">Privacy Policy</a></li>
                           <li><a href="#">FAQ</a></li>
                            <li><a href="#">Press</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Resources</h5>
                    <ul class="f-li">
                        <li><a href="#">Why RTG?</a></li>
                         <li><a href="#">Blog</a></li>
                          <li><a href="#">RTG Points</a></li>
                           <li><a href="#">Promotions</a></li>
                            <li><a href="#">Student Store</a></li>
                    </ul>
                </div>
                  <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Work with RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">Become a Supplier</a></li>
                         <li><a href="#">Distributor Collaboration</a></li>
                          <li><a href="#">Affiliate Program</a></li>
                           <li><a href="#">Careers</a></li>
                            <li><a href="#">Influencer Partnerships</a></li>
                             <li><a href="#">Selected Promotions</a></li>
                    </ul>
                </div>
                   <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Contact Us</h5>
                    <ul class="f-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
                </div>
        </div>
        </div> 
        <p class="copy-text">COPYRIGHT © 2020 RTG All rights reserved</p>
    </div>
</footer>


<!-- Footer end -->



  
	<script src="{{ asset('theme1/js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('theme1/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('theme1/js/custom.js') }}"></script>

</body>
</html>
