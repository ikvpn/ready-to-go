<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ready to go</title>
	<link href="{{ asset('theme1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('theme1/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/responsive.css') }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
</head>
<body>


<!-- header start -->    
<div class="top-container">
  <div class="container">
    <ul class="t-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
    <div class="title-section">
      <div class="row">
        <div class="col-lg-3 col-sm-12"></div>
        <div class="col-lg-6 col-sm-12">
      <h1>@lang('blog/index.RTG Travel Blog')</h1>
      <p>@lang('blog/index.The state of Utah in the United States is home to lots of beautiful National Parks, & Bryce Canyon National Park ranks as three of the most magnificent & awe inspiring.')</p>
        </div>
        <div class="col-lg-3 col-sm-12"></div>
      </div>
    </div>
    <a href="#myHeader"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
  </div>
</div>

<div class="header" id="myHeader">
 <div class="container">
  <div class="row">
  <div class="col-lg-1"></div>
  <div class="col-lg-10 text-center">
   <nav class="navbar navbar-expand-lg navbar-light d-inline-block">     
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button> 
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        @foreach($countries as $country)
            <li class="nav-item {{ Request::segment(2) === $country->country ? 'active' : null }}">
              <a class="nav-link" href="{{ url('blog',$country->country) }}">@if(app()->getLocale() == 'en') {{ strtoupper($country->country) }} @else {{ $country->country_zh }} @endif</a>
            </li>
        @endforeach
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/') }}">@lang('blog/index.RTG WEBSTIE')</a>
        </li> 
      </ul>    
    </div>
 
  </nav>
</div>
<div class="col-lg-1"></div>
</div>
 </div>
</div>

<!-- header end -->

<!-- mesnory grid srart -->
<section class="grid-section">
   <div class="container">
    <div class="row">
    	@foreach($blogs as $blog)
      	<div class="col-lg-4 col-md-6 col-sm-12">
	        <div class="grid-part">
	        	<img src="{{ $blog->image }}" alt="Avatar" class="image">
	        	<div class="card-body text-center">
	                <h6>@lang('blog/index.DESTINATIONS')</h6>
	                <h4 class="card-title">@if(app()->getLocale() == 'en') {{ $blog->title }} @else {{ $blog->title_zh }} @endif</h4>
	                <p class="card-text">@if(app()->getLocale() == 'en') {{ $blog->discription }} @else {{ $blog->discription_zh }} @endif</p>
	            </div>
		        <div class="overlay">
		          <div class="text">@lang('blog/index.ANDRE GIDE')</div>
		          <P class="text2">@if(app()->getLocale() == 'en') {{ $blog->content }} @else {{ $blog->content_zh }} @endif</P>
		        </div>
	        </div>
      	</div>
      	@endforeach
    </div>
    <nav aria-label="...">
	  <ul class="pagination pagination-section float-right">   
	    <!-- <li class="page-item active"><a class="page-link" href="#">1</a></li>
	    <li class="page-item ">
	      <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
	    </li>
	    <li class="page-item"><a class="page-link" href="#">3</a></li>
	    <li class="page-item"><a class="page-link" href="#">4</a></li>
	    <li class="page-item"><a class="page-link" href="#">5</a></li>
	    <li class="page-item"><a class="page-link" href="#">6</a></li> -->
	    
		{!! $blogs->links() !!}
	  </ul>
	</nav>
  <a href="{{ route('blog') }}" class="btn btn-default mt-5">@lang('blog/index.More')</a>
   <!--  <div class="card-columns">
        <div class="card">
            <img class="card-img-top img-fluid" src="https://www.thebalancesmb.com/thmb/9-TaSUt-qCdOp1Xh3P43mutTmeA=/2121x1414/filters:fill(auto,1)/GettyImages-887987150-5c770377c9e77c00011c82e6.jpg" alt="Card image cap">
            <div class="card-body text-center">
                <h6>DESTINATIONS</h6>
                <h4 class="card-title">Traveling To Barcelona</h4>
                <p class="card-text">There are not many cities that have experienced such social and political extremes in recent
                history as Amsterdam.</p>
            </div>
        </div>

        <div class="card card-body">
            <blockquote class="card-blockquote">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                <footer>
                    <small class="text-muted">
          Someone famous in <cite title="Source Title">Source Title</cite>
        </small>
                </footer>
            </blockquote>
        </div>
        <div class="card">
            <img class="card-img-top img-fluid" src="https://www.thebalancesmb.com/thmb/9-TaSUt-qCdOp1Xh3P43mutTmeA=/2121x1414/filters:fill(auto,1)/GettyImages-887987150-5c770377c9e77c00011c82e6.jpg" alt="Card image cap">
            <div class="card-body">
                <h4 class="card-title">Card title</h4>
                <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
        <div class="card card-body card-inverse card-primary text-xs-center">
            <blockquote class="card-blockquote">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
                <footer>
                    <small>
          Someone famous in <cite title="Source Title">Source Title</cite>
        </small>
                </footer>
            </blockquote>
        </div>
        <div class="card card-body text-xs-center">
            <h4 class="card-title">Card title</h4>
            <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
        <div class="card">
            <img class="card-img img-fluid" src="https://www.thebalancesmb.com/thmb/9-TaSUt-qCdOp1Xh3P43mutTmeA=/2121x1414/filters:fill(auto,1)/GettyImages-887987150-5c770377c9e77c00011c82e6.jpg" alt="Card image">
        </div>
        <div class="card card-body text-right">
            <blockquote class="card-blockquote">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                <footer>
                   <img class="card-img img-fluid" src="https://www.onblastblog.com/wp-content/uploads/2018/05/free-images-for-blogs.jpeg">
                </footer>
            </blockquote>
        </div>
        <div class="card card-body">
            <h4 class="card-title">Card title</h4>
            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
    </div> -->

</div>  
</section>

<!-- service-section end -->



<!-- Footer start -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="top-container">
                   <ul class="t-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="copy-part">
                <p class="copy-text">COPYRIGHT © 2020 RTG All rights reserved</p>
              </div>
            </div>
        </div>       
        
    </div>
</footer>


<!-- Footer end -->



  
	<script src="{{ asset('theme1/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('theme1/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('theme1/js/custom.js') }}"></script>

</body>
</html>
