<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>header</title>
    <link href="{{ asset('theme1/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/responsive.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/slick.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/slick-theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('theme1/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">   
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- <link rel="stylesheet" type="text/css" href="css/style.css"> -->
    
</head>
<body>
<!-- header start -->    

<header class="sticky-top">
<nav class="navbar navbar-expand-lg navbar-light">
     <div class="container">
  <a class="navbar-brand" href="#"><img src="images/logo.png" alt=""></a></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> 
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Parnter</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">Newsroom</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">FAQ</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        @if(app()->getLocale() == 'en') English @else Chinese @endif
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          @if(app()->getLocale() == 'en')
            <a class="dropdown-item" href="{{ url('locale/zh') }}">Chinese</a>
          @else
            <a class="dropdown-item" href="{{ url('locale/en') }}">English</a>
          @endif
        </div>
      </li>     
    </ul>    
  </div>
</div>
</nav>
</header>

<!-- header end -->

<!-- hero-section-1 start -->

<section class="w-rtg-part">
<div class="container"> 
      
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                      <div class="hero-part-1">
                      <h1>{{ __('cms/support.Welcome to RTG support') }}</h1>
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="@lang('cms/support.Search by support')">
                        <div class="input-group-append">
                          <button class="btn btn-search" type="button">
                            <i class="fa fa-search" aria-hidden="true"></i>
                          </button>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
</div>
</section>

<!-- hero-section-1 end -->

<!-- service-section srart -->
<section class="service-part p-60">
    <div class="container"> 
        <div class="row">            
            @foreach($supports as $support)
            <div class="col-lg-4 col-sm-6">
                <div class="icon-part pb-90">
                    <img src="{{ asset('theme1/images/supports/'.$support->image) }}" alt="icon">
                    <h5>@if(app()->getLocale() == 'en') {{ $support->name }} @else {{ $support->name_zh }} @endif</h5>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<!-- service-section end -->

<!-- Footer start -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="news-text">
                    <p>{{ __('cms/support.Subscribe to newsletter of RTG to get the latest promos, exclusive discounts and helpful travel tips!') }}</p>
                    <p>{{ __('cms/support.You agree to our') }}<a href="#"> {{ __('cms/support.Terms & Conditions') }}</a> {{ __('cms/support.and') }} <a href="#">{{ __('cms/support.Privacy Policy') }}</a> {{ __('cms/support.when you click Subscribe') }}</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <form class="subscribe_form">
                    <div class="input-group">
                       <input type="text" class="form-control" name="email" placeholder="{{ __('cms/support.Please enter your email address!') }}">
                       <span class="input-group-btn">
                            <button class="btn btn-default" type="button">{{__('cms/support.subscribe')}}</button>
                       </span>
                    </div>
                </form>
            </div>
        </div>
         <div class="border-tb"> 
        <div class="row">                 
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">About Us</a></li>
                         <li><a href="#">Terms & Conditions</a></li>
                          <li><a href="#">Privacy Policy</a></li>
                           <li><a href="#">FAQ</a></li>
                            <li><a href="#">Press</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Resources</h5>
                    <ul class="f-li">
                        <li><a href="#">Why RTG?</a></li>
                         <li><a href="#">Blog</a></li>
                          <li><a href="#">RTG Points</a></li>
                           <li><a href="#">Promotions</a></li>
                            <li><a href="#">Student Store</a></li>
                    </ul>
                </div>
                  <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Work with RTG</h5>
                    <ul class="f-li">
                        <li><a href="#">Become a Supplier</a></li>
                         <li><a href="#">Distributor Collaboration</a></li>
                          <li><a href="#">Affiliate Program</a></li>
                           <li><a href="#">Careers</a></li>
                            <li><a href="#">Influencer Partnerships</a></li>
                             <li><a href="#">Selected Promotions</a></li>
                    </ul>
                </div>
                   <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5 class="f-title">Contact Us</h5>
                    <ul class="f-social">
                        <li class="d-inline"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
                        <li class="d-inline"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            
                    </ul>
                </div>
        </div>
        </div> 
        <p class="copy-text">COPYRIGHT © 2020 RTG All rights reserved</p>
    </div>
</footer>
<!-- Footer end -->

  <script src="{{ asset('theme1/js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('theme1/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('theme1/js/custom.js') }}"></script>
  <script src="{{ asset('theme1/js/slick.min.js') }}"></script>

  <script type="text/javascript">
      
    $('#search').click(function(){
      var search = $('#search_bar').val();
      if(search == '')
      {
        search = 'all_blank';
      }
      var url = "{{ url('support/search/') }}"+'/'+search;
      $.ajax({
        type: 'GET',
        url: url,
        success: function(data)
        { 
          $('#all_support').html(data);
        }

      });
    });

  </script>


</body>
</html>
