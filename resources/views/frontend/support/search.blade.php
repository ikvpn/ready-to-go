@foreach($supports as $support)
<div class="col-lg-4 col-sm-6">
    <div class="icon-part pb-90">
        <img src="{{ asset('theme1/images/supports/'.$support->image) }}" alt="icon">
        <h5>{{ $support->name }}</h5>
    </div>
</div>
@endforeach