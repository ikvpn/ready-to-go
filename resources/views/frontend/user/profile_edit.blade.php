@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Edit User profile</div>

                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" action="{{ route('user.profile.update') }}" id="sign_up_form">
                        @csrf
                        <div class="form-group">
                            <label>User Name:</label>
                            <input type="text" name="username" id="username" class="form-control" value="{{ $user->username }}">
                            @if($errors->has('username'))
                                <span class="text-danger">{{ $errors->first('username') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Full Name:</label>
                            <input type="text" name="fullname" id="fullname" class="form-control" value="{{ $user->fullname }}">
                            @if($errors->has('fullname'))
                                <span class="text-danger">{{ $errors->first('fullname') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Email:</label>
                            <input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}" readonly>
                            @if($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Gender:</label>
                            <input type="radio" name="gender" value="male" @if($user->gender == 'male') checked @endif> Male
                            <input type="radio" name="gender" value="female" @if($user->gender == 'female') checked @endif> Female<br>
                            @if($errors->has('gender'))
                                <span class="text-danger">{{ $errors->first('gender') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Mobile No:</label><br>
                            <input type="text" name="mobile_no" id="mobile_no" class="form-control" value="{{ $user->mobile_no }}">
                            @if($errors->has('mobile_no'))
                                <span class="text-danger">{{ $errors->first('mobile_no') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Profile:</label>
                            <input type="file" name="profile" id="profile">
                            <?php if (@GetImageSize($user->profile)) { ?>
                                <img src="{{ $user->profile }}" width="50" height="50">
                            <?php } else { ?>
                                <img src="{{ asset('profile/users/'.$user->profile) }}" width="50" height="50">
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-success">Update</button>
                            </div>
                        </div>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
