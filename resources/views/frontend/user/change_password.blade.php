@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Change password</div>

                <div class="card-body">
                	@if(Session::get('success'))
                		<div class="alert alert-success">{{ Session::get('success') }}</div>
                	@endif
                	@if(Session::get('error'))
                		<div class="alert alert-danger">{{ Session::get('error') }}</div>
                	@endif
                    <form method="post" enctype="multipart/form-data" action="{{ route('user.change_password.submit') }}" id="sign_up_form">
                        @csrf
                        <div class="form-group">
                            <label>Old password:</label>
                            <input type="password" name="old_password" id="old_password" class="form-control" placeholder="Enter current password">
                            @if($errors->has('old_password'))
                                <span class="text-danger">{{ $errors->first('old_password') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>New Password:</label>
                            <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter new password">
                            @if($errors->has('new_password'))
                                <span class="text-danger">{{ $errors->first('new_password') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Confirm New Password:</label>
                            <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control" placeholder="Enter confirm new password">
                            @if($errors->has('confirm_new_password'))
                                <span class="text-danger">{{ $errors->first('confirm_new_password') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-success">Update</button>
                            </div>
                        </div>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
