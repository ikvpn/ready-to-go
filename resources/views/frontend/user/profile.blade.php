@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">User profile</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            User Name: <b>{{ $user->username }}</b>
                        </div>
                        <div class="col-md-6">
                            Full Name: <b>{{ $user->fullname }}</b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            Email: <b>{{ $user->email }}</b>
                        </div>
                        <div class="col-md-6">
                            Gender: <b>{{ $user->gender }}</b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            Mobile No: <b>{{ $user->mobile_no }}</b>
                        </div>
                        <div class="col-md-6">Profile:
                            <?php if (@GetImageSize($user->profile)) { ?>
                                <img src="{{ $user->profile }}" width="50" height="50">
                            <?php } else { ?>
                                <img src="{{ asset('profile/users/'.$user->profile) }}" width="50" height="50"></b>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="{{ route('user.profile.edit') }}" class="btn btn-success">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
