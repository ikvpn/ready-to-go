<?php  

	return [

		'RTG Travel Blog'								=>	'RTG旅遊博客',
		'The state of Utah in the United States is home to lots of beautiful National Parks, & Bryce Canyon National Park ranks as three of the most magnificent & awe inspiring.' => '美國猶他州擁有許多美麗的國家公園，布萊斯峽谷國家公園是最宏偉和令人敬畏的三個國家之一。',
		'RTG WEBSTIE'									=>	'RTGS網站',
		'DESTINATIONS'									=>	'目的地',
		'ANDRE GIDE'									=>	'安德烈·吉德	',
		'More'											=>	'更多'
			
	];

?>