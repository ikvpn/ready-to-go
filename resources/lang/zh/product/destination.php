<?php 

	return [

		'booked'												=>	'已预订',
		'Earliest Booking Date :'								=>	'最早预订日期：',
		'Instant Confirmation'									=>	'即时确认',
		'Free cancellation'										=>	'免费取消',
		'experiences found'										=>	'发现的经验',
		'Sort by'												=>	'排序方式',
		'RTG suggestions'										=>	'RTG建议',
		'Popularity'											=>	'人气度',
		'Ratings'												=>	'等级',
		'Price: low to high'									=>	'价格：从低到高'

	];

?>