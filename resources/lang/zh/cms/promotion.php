<?php

return [

	'RTG Points'											=>	'RTG點數',
	'RTG Points Member Exclusive Offers'					=>	'RTG積分會員獨家優惠',
	'Welcome Offer'											=>	'迎新優惠',
	'Redeem'												=>	'贖回',
	'Your total RTG Points :'								=>	'您的RTG總積分：',
	'More'													=>	'更多',
	'Download our app !'									=>	'下載我們的應用程序！',
	'Earned points'											=>	'賺取積分',
	'points'												=>	'點數',
	'Gift quantity :'										=>	'禮品數量：',
	'Points'												=>	'點數',

];

?>