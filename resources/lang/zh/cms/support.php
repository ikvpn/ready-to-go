<?php

return [

    'Welcome to RTG support'		=>  '歡迎使用RTG支持',
    'Search by support'				=>	'按支持搜索',
    'Subscribe to newsletter of RTG to get the latest promos, exclusive discounts and helpful travel tips!' => '訂閱RTG時事通訊以獲得最新促銷，獨家折扣和有用的旅行提示！',
    'You agree to our'				=>	'您同意我們的',		
    'Terms & Conditions'			=>	'條款及細則',
    'and'							=>	'和',
    'Privacy Policy'				=>	'隱私政策',
    'when you click Subscribe'		=>	'當您單擊訂閱時',
    'Please enter your email address!'	=>	'請輸入您的電子郵件地址！',
    'subscribe'						=>	'訂閱',


];

?>