<?php

return [

	'Why RTG ?'							=>	'為什麼選擇RTG？',
	'See why millions of travelers choose to experience the world as part of our strong and secure RTG community.' => '了解為什麼數以百萬計的旅行者選擇在我們強大而安全的RTG社區中體驗世界。',
	'A secure platform accessible from multiple devices'	=>	'可從多個設備訪問的安全平台',
	'Safety and Encryption'				=>	'安全與加密',
	'Safety and security is of utmost importance to us. Your personal information along with payment methods are kept highly encrypted and will never be shared with a third party.'	=>	'安全與保障對我們至關重要。您的個人信息以及付款方式都經過高度加密，絕不會與第三方共享。',
	'Seamless and Secure'				=>	'無縫且安全',
	"Travelers pay through KKday when they make a booking. After the tour or experience is over the service provider will receive payment directly from KKday. We'll manage the transactions while you go out and have fun."	=>	'旅行者在預訂時通過KKday付款。遊覽或體驗結束後，服務提供商將直接從KKday收到付款。當您外出玩樂時，我們將管理交易。',
	'Browse and book anywhere'			=>	'隨處瀏覽和預訂',
	"It doesn't matter if you like to plan ahead or if you are the spontaneous type. Have the freedom to make secure bookings on the go and get instant confirmations." => '無論您想提前計劃還是自髮型，都沒關係。可以隨時隨地進行安全的預訂並獲得即時確認。',
	'Experience the freedom to discover and book what you never knew was possible !' => '體驗發現和預訂您從未知道的可能性的自由！',
	"We've done the hard work so you can access the best your destination has to offer. Don't just take the road less taken- determine your own journey with us."	=>	'我們已經完成了艱苦的工作，因此您可以訪問目的地所能提供的最佳服務。不要只走少走的路-確定您自己的旅程。',
	'Explore NOW !'						=>	'立即探索！',

];

?>