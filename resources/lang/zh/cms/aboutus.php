<?php

	return [

		'About Us'											=>	'關於我們',
		"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard" => "Lorem Ipsum只是印刷和排版行業的偽文本。 Lorem Ipsum一直是行業標準",
		'Founder'											=>	'創辦人'	,
		"Founder's name"									=>	"創辦人姓名",
		"Founder's info"									=>	"創始人信息",
		'50+ years experience'								=>	'超過50年的經驗',
		'100+ Local Tour'									=>	'100+本地遊',
		'500+ Cities'										=>	'500多個城市',
		'Location'											=>	'位置',
		'HONG KONG'											=>	'香港'

	];

?>