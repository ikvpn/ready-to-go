<?php

return [

	'Starting your journey with RTG Points !'				=>	'使用RTG積分開始您的旅程！',
	'RTG Points FAQs'										=>	'RTG積分常見問題解答',
	'RTG reserves the right to change, modify and/or eliminate RTG Points and/or all or any portion of these Terms of Use or any policy, FAQ, or guideline pertaining to RTG Points at any time and at its sole discretion. Violations of the terms of use/any policy or abuse of RTG Points usage will result in termination of your account.' => 'RTG保留隨時自行決定更改，修改和/或消除RTG積分和/或這些使用條款的全部或任何部分或與RTG積分有關的任何政策，常見問題或指南的權利。違反使用條款/任何政策或濫用RTG積分會導致您的帳戶被終止。',

];

?>