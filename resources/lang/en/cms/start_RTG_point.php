<?php

return [

	'Starting your journey with RTG Points !'				=>	'Starting your journey with RTG Points !',
	'RTG Points FAQs'										=>	'RTG Points FAQs',
	'RTG reserves the right to change, modify and/or eliminate RTG Points and/or all or any portion of these Terms of Use or any policy, FAQ, or guideline pertaining to RTG Points at any time and at its sole discretion. Violations of the terms of use/any policy or abuse of RTG Points usage will result in termination of your account.' => 'RTG reserves the right to change, modify and/or eliminate RTG Points and/or all or any portion of these Terms of Use or any policy, FAQ, or guideline pertaining to RTG Points at any time and at its sole discretion. Violations of the terms of use/any policy or abuse of RTG Points usage will result in termination of your account.',
	
];

?>