<?php

return [

    'Welcome to RTG support'        =>  'Welcome to RTG support',
    'Search by support'				=>	'Search by support',
    'Subscribe to newsletter of RTG to get the latest promos, exclusive discounts and helpful travel tips!' => 'Subscribe to newsletter of RTG to get the latest promos, exclusive discounts and helpful travel tips!',
    'You agree to our'				=>	'You agree to our',
    'Terms & Conditions'			=>	'Terms & Conditions',
    'and'							=>	'and',
    'Privacy Policy'				=>	'Privacy Policy',
    'when you click Subscribe'		=>	'when you click Subscribe',
    'Please enter your email address!'	=>	'Please enter your email address!',
    'subscribe'						=>	'subscribe',
    
];

?>