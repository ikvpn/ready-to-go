<?php

return [

	'RTG Points'										=>	'RTG Points',
	'RTG Points Member Exclusive Offers'				=>	'RTG Points Member Exclusive Offers',
	'Welcome Offer'										=>	'Welcome Offer',
	'Redeem'											=>	'Redeem',
	'Your total RTG Points :'							=>	'Your total RTG Points :',
	'More'												=>	'More',
	'Download our app !'								=>	'Download our app !',
	'Earned points'										=>	'Earned points',
	'points'											=>	'points',
	'Gift quantity :'									=>	'Gift quantity :',
	'Points'											=>	'Points',

];

?>