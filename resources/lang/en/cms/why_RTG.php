<?php

return [

	'Why RTG ?'							=>	'Why RTG ?',
	'See why millions of travelers choose to experience the world as part of our strong and secure RTG community.' => 'See why millions of travelers choose to experience the world as part of our strong and secure RTG community.',
	'A secure platform accessible from multiple devices'	=>	'A secure platform accessible from multiple devices',
	'Safety and Encryption'	=>	'Safety and Encryption',
	'Safety and security is of utmost importance to us. Your personal information along with payment methods are kept highly encrypted and will never be shared with a third party.'	=>	'Safety and security is of utmost importance to us. Your personal information along with payment methods are kept highly encrypted and will never be shared with a third party.',
	'Seamless and Secure'				=>	'Seamless and Secure',
	"Travelers pay through KKday when they make a booking. After the tour or experience is over the service provider will receive payment directly from KKday. We'll manage the transactions while you go out and have fun." => "Travelers pay through KKday when they make a booking. After the tour or experience is over the service provider will receive payment directly from KKday. We'll manage the transactions while you go out and have fun.",
	'Browse and book anywhere'	=>	'Browse and book anywhere',
	"It doesn't matter if you like to plan ahead or if you are the spontaneous type. Have the freedom to make secure bookings on the go and get instant confirmations." => "It doesn't matter if you like to plan ahead or if you are the spontaneous type. Have the freedom to make secure bookings on the go and get instant confirmations.",
	'Experience the freedom to discover and book what you never knew was possible !'	=>	'Experience the freedom to discover and book what you never knew was possible !',
	"We've done the hard work so you can access the best your destination has to offer. Don't just take the road less taken- determine your own journey with us."	=> "We've done the hard work so you can access the best your destination has to offer. Don't just take the road less taken- determine your own journey with us.",
	'Explore NOW !'						=>	'Explore NOW !',
	
];

?>