<?php 

	return [

		'booked'										=>	'booked',
		'Earliest Booking Date :'						=>	'Earliest Booking Date :',
		'Instant Confirmation'							=>	'Instant Confirmation',
		'Free cancellation'								=>	'Free cancellation',
		'experiences found'								=>	'experiences found',
		'Sort by'										=>	'Sort by',
		'RTG suggestions'								=>	'RTG suggestions',
		'Popularity'									=>	'Popularity',
		'Ratings'										=>	'Ratings',
		'Price: low to high'							=>	'Price: low to high',

	];

?>